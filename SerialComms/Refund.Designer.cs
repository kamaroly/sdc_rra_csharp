﻿namespace SDCCommunicator
{
    partial class formRefund
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FormRefundInput = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // FormRefundInput
            // 
            this.FormRefundInput.AutoSize = true;
            this.FormRefundInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormRefundInput.Location = new System.Drawing.Point(66, 21);
            this.FormRefundInput.Name = "FormRefundInput";
            this.FormRefundInput.Size = new System.Drawing.Size(245, 17);
            this.FormRefundInput.TabIndex = 0;
            this.FormRefundInput.Text = "Enter invoice number to proceed";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(69, 41);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(242, 20);
            this.textBox1.TabIndex = 1;
            // 
            // formRefund
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 90);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.FormRefundInput);
            this.Name = "formRefund";
            this.Text = "Refund";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FormRefundInput;
        private System.Windows.Forms.TextBox textBox1;
    }
}