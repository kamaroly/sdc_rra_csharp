﻿using SDCCommunicator.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDCCommunicator.Repository
{
    class SdcRepository
    {
        string connectionString = string.Empty;
        string TIN = string.Empty;
        string MRC = string.Empty;
        string pathName = System.Environment.CurrentDirectory;
        INIFile inif = new INIFile("\\config.ini");

        public SdcRepository()
        {
            this.inif = new INIFile(this.pathName + "\\config.ini");
            this.connectionString = connectionString =
                    "server=" + this.inif.Read("DATABASE", "DBHOST") + ";" +
                    "initial catalog=" + this.inif.Read("DATABASE", "DBNAME") + ";" +
                    "user id=" + this.inif.Read("DATABASE", "DBUSER") + ";" +
                    "password=" + this.inif.Read("DATABASE", "DBPASS") + "";

            this.MRC = this.inif.Read("SDC", "MRC");
            this.TIN = this.inif.Read("SDC", "TIN");
        }

        public Dictionary<string,string>  Read()
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            try
            {
                using (SqlConnection conn =
                    new SqlConnection(this.connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd =
                        new SqlCommand("SELECT * FROM [sdc_test]", conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                           
                        }

                        reader.Close();
                    }
                }

                return response;
            }
            catch (SqlException ex)
            {
                //Log exception
                //Display Error message
                return response;
            }
        }

        public void Insert()
        {
            try
            {
             
                using (SqlConnection conn =
                    new SqlConnection(this.connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd =
                        new SqlCommand("INSERT INTO EmployeeDetails VALUES(" +
                            "@Id, @Name, @Address)", conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", 1);
                        cmd.Parameters.AddWithValue("@Name", "Amal Hashim");
                        cmd.Parameters.AddWithValue("@Address", "Bangalore");

                        int rows = cmd.ExecuteNonQuery();

                        //rows number of record got inserted
                    }
                }
            }
            catch (SqlException ex)
            {
                //Log exception
                //Display Error message
            }
        }

        public void Update()
        {
            try
            {
              
                using (SqlConnection conn =
                    new SqlConnection(this.connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd =
                        new SqlCommand("UPDATE EmployeeDetails SET Name=@NewName, Address=@NewAddress" +
                            " WHERE Id=@Id", conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", 1);
                        cmd.Parameters.AddWithValue("@Name", "Munna Hussain");
                        cmd.Parameters.AddWithValue("@Address", "Kerala");

                        int rows = cmd.ExecuteNonQuery();

                        //rows number of record got updated
                    }
                }
            }
            catch (SqlException ex)
            {
                //Log exception
                //Display Error message
            }
        }

        public void Delete()
        {
            try
            {
              
                using (SqlConnection conn =
                    new SqlConnection(this.connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd =
                        new SqlCommand("DELETE FROM EmployeeDetails " +
                            "WHERE Id=@Id", conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", 1);

                        int rows = cmd.ExecuteNonQuery();

                        //rows number of record got deleted
                    }
                }
            }
            catch (SqlException ex)
            {
                //Log exception
                //Display Error message
            }
        }
    }
}
