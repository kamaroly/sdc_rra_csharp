﻿using SDCCommunicator.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDCCommunicator.Models
{
    class ReportModel
    {
         private static string CONNECTION_STRING = string.Empty;
      
        
        /**
         * Set connection string dynamically
         */
        public static string getConnectionString()
        {
            string path = System.Environment.CurrentDirectory +  "\\" + Properties.Settings.Default.SETTINGS_FILE_PATH;
            INIFile inifile = new INIFile(path);
            CONNECTION_STRING = Properties.Settings.Default.CONNECTION_STRING;
            string test = inifile.Read("DATABASE", "DBHOST").ToString();
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_HOST", Convert.ToString(inifile.Read("DATABASE", "DBHOST")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_NAME", Convert.ToString(inifile.Read("DATABASE", "DBNAME")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_USER", Convert.ToString(inifile.Read("DATABASE", "DBUSER")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_PASSWORD", Convert.ToString(inifile.Read("DATABASE", "DBPASS")));

            return CONNECTION_STRING;
        }

        // GETTING XZ REPORT
        public static List<ReportItem> getZxrepot(string startDate, string endDate)
        {
            List<ReportItem> report = new List<ReportItem>();
            ReportItem reportRow = new ReportItem();

             using (SqlConnection connection = new SqlConnection(getConnectionString()))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand( "sp_ZXReport", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@STARTDATE", startDate);
                    command.Parameters.AddWithValue("@ENDDATE", endDate);
                    SqlDataReader reader = command.ExecuteReader();
                   
                    while (reader.Read())
                    {
                        reportRow = new ReportItem();
                        reportRow.item = reader["item"].ToString();
                        reportRow.value = reader["value"].ToString();
                        report.Add(reportRow);
                    }    
                }

            return report;
        }

        // GETTING PLU REPORT
        public static List<ReportItem> getPLUreport(string startDate, string endDate)
        {
            List<ReportItem> report = new List<ReportItem>();
            ReportItem reportRow = new ReportItem();

            using (SqlConnection connection = new SqlConnection(getConnectionString()))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_PLUReport", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@STARTDATE", startDate);
                command.Parameters.AddWithValue("@ENDDATE", endDate);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    reportRow = new ReportItem();
                    reportRow.item = reader["item"].ToString();
                    reportRow.value = reader["value"].ToString();
                    report.Add(reportRow);
                }
            }

            return report;
        }
    }
}
