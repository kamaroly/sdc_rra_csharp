﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDCCommunicator.Models
{
    class ReportItem
    {
        public string item { set; get; }
        public string value { set; get; }
    }
}
