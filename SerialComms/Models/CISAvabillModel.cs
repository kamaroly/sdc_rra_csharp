﻿using SDCCommunicator.Helpers;
using SerialComms.Logs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDCCommunicator.Models
{
    class CISAvabillModel
    {
        public string ID { set; get; }
        public string ACCOUNT_NUMBER { set; get; }
        public string ACCOUNT_NAME { set; get; }
        public string NORMAL_SALE_REFERENCE_NUMBER { set; get; }
        public string CUSTOMER_ADDRESS { set; get; }
        public string TYPE_OF_ACCOUNT { set; get; }
        public string PAYMENT_DUE_DATE { set; get; }
        public string INVOICE_NUMBER { set; get; }
        public string Balance_Brought_Forward { set; get; }
        public string Adjustments { set; get; }
        public string Payments_Received { set; get; }
        public string TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS { set; get; }
        public string MONTHLY_CHARGES { set; get; }
        public string Total_Call_Entitlement_Charges { set; get; }
        public string CUG_and_Feature_Charges { set; get; }
        public string Printed_Bill { set; get; }
        public string Late_Charges { set; get; }
        public string Other_Charges { set; get; }
        public string Discounts { set; get; }
        public string TOTAL_CHARGES_FOR_THE_MONTH { set; get; }
        public string TOTAL_AMOUNT_DUE { set; get; }
        public string Total_Additional_Recharges { set; get; }
        public string Voice_Local { set; get; }
        public string SMS { set; get; }
        public string DATA { set; get; }
        public string VAS { set; get; }
        public string Voice_International { set; get; }
        public string Roaming { set; get; }
        public string TOTAL { set; get; }
        public string TOTAL_B_18 { set; get; }
        public string TOTAL_TAX_B { set; get; }
        public string TOTAL_TAX { set; get; }
        public string CASH { set; get; }
        public string ITEMS_NUMBER { set; get; }
        public string SDC_DATE { set; get; }
        public string SDC_TIME { set; get; }
        public string SDC_ID { set; get; }
        public string SDC_RECEIPT_NUMBER { set; get; }
        public string INTERNAL_DATA { set; get; }
        public string RECEIPT_SIGNATURE { set; get; }
        public string CIS_DATE { set; get; }
        public string CIS_TIME { set; get; }
        public string RECEIPT_TYPE_MRC { set; get; }
        public string TIN { set; get; }
        public string RECEIPT_NUMBER { set; get; }
        public string TAXRATE1 { set; get; }
        public string TAXRRATE2 { set; get; }
        public string TAXRATE3 { set; get; }
        public string TAXRATE4 { set; get; }
        public string AMOUNT1 { set; get; }
        public string AMOUNT2 { set; get; }
        public string AMOUNT3 { set; get; }
        public string AMOUNT4 { set; get; }
        public string TAX1 { set; get; }
        public string TAX2 { set; get; }
        public string TAX3 { set; get; }
        public string TAX4 { set; get; }
        public string MRC { set; get; }
        public string STATUS { set; get; }
        public string RESULTS { set; get; }
        public string CREATED_AT { set; get; }
        public string UPDATED_D_AT { set; get; }

        private static string CONNECTION_STRING = string.Empty;
      
        
        /**
         * Set connection string dynamically
         */
        public static string getConnectionString()
        {
            string path = System.Environment.CurrentDirectory +  "\\" + Properties.Settings.Default.SETTINGS_FILE_PATH;
            INIFile inifile = new INIFile(path);
            CONNECTION_STRING = Properties.Settings.Default.CONNECTION_STRING;
            string test = inifile.Read("DATABASE", "DBHOST").ToString();
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_HOST", Convert.ToString(inifile.Read("DATABASE", "DBHOST")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_NAME", Convert.ToString(inifile.Read("DATABASE", "DBNAME")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_USER", Convert.ToString(inifile.Read("DATABASE", "DBUSER")));
            CONNECTION_STRING = CONNECTION_STRING.Replace("PARAM_DB_PASSWORD", Convert.ToString(inifile.Read("DATABASE", "DBPASS")));

            return CONNECTION_STRING;
        }

        /**
         * UPDATE THE TRANSACTIONS 
         */ 
        public static void updateInvoice(CISAvabillModel invoice)
        {
            string query = "UPDATE [SDC_AVABILL_INVOICE_INFORMATION] SET [SDC_DATE]=@SDC_DATE,[SDC_TIME]=@SDC_TIME,[SDC_ID]=@SDC_ID,[SDC_RECEIPT_NUMBER]=@SDC_RECEIPT_NUMBER,[INTERNAL_DATA]=@INTERNAL_DATA,[RECEIPT_SIGNATURE]=@RECEIPT_SIGNATURE,[STATUS]=@STATUS,[RESULTS]=@RESULTS,[MRC]=@MRC,[CIS_DATE]=@CIS_DATE, [CIS_TIME]=@CIS_TIME,[RECEIPT_TYPE_MRC]=@RECEIPT_TYPE_MRC,[TIN]=@TIN,[RECEIPT_NUMBER]=@RECEIPT_NUMBER,[UPDATED_D_AT]=GETDATE() WHERE ID = @ID;";

            using (SqlConnection connection = new SqlConnection(getConnectionString()))
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    command.Parameters.AddWithValue("STATUS", invoice.STATUS);
                    command.Parameters.AddWithValue("SDC_ID", invoice.SDC_ID);
                    command.Parameters.AddWithValue("RESULTS", invoice.RESULTS);
                    command.Parameters.AddWithValue("SDC_TIME", invoice.SDC_TIME);
                    command.Parameters.AddWithValue("SDC_DATE", invoice.SDC_DATE);
                    command.Parameters.AddWithValue("CIS_DATE", invoice.CIS_DATE);
                    command.Parameters.AddWithValue("CIS_TIME", invoice.CIS_TIME);
                    command.Parameters.AddWithValue("INTERNAL_DATA", invoice.INTERNAL_DATA);
                    command.Parameters.AddWithValue("RECEIPT_SIGNATURE", invoice.RECEIPT_SIGNATURE);
                    command.Parameters.AddWithValue("SDC_RECEIPT_NUMBER", invoice.SDC_RECEIPT_NUMBER);
                    command.Parameters.AddWithValue("MRC", invoice.MRC);
                    command.Parameters.AddWithValue("RECEIPT_TYPE_MRC", invoice.RECEIPT_TYPE_MRC);
                    command.Parameters.AddWithValue("TIN", invoice.TIN);
                    command.Parameters.AddWithValue("RECEIPT_NUMBER", invoice.RECEIPT_NUMBER);
                    command.Parameters.AddWithValue("ID", invoice.ID);
                    
                    try
                    {
                        connection.Open();

                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        Logger.critical(ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        /**
         * Get next ID from the db
         */
        public static string getNextId()
        {
             // Distinguish queries
            string query =  "SELECT  MAX(ID) AS ID FROM [SDC_AVABILL_INVOICE_INFORMATION] WITH(NOLOCK)";
            string Id  = "0";
            try
            {
                using (SqlConnection connection = new SqlConnection(getConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query,connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        
                        while (reader.Read())
                        {
                          int ID = Convert.ToInt32(reader["ID"]) + 1;
                          
                          return ID.ToString();
                        } 
                    }
                }
            }
            catch (Exception exception)
            {
                // Something went wrong try to log it.
                Logger.error(exception.Message);
   
            }
            return Id;
        }

        /**
         * Pick invoice to process
         */
        public static   List<CISAvabillModel> pickInvoices(string invoiceNumber = null,bool Sale = false)
        {
            List<CISAvabillModel> invoices = new List<CISAvabillModel>();
            // Distinguish queries
            string query =  "SELECT  * FROM [SDC].[dbo].[SDC_AVABILL_INVOICE_INFORMATION] WITH(NOLOCK) WHERE INTERNAL_DATA IS NULL OR RTRIM(LTRIM(INTERNAL_DATA))='';";

            if (invoiceNumber != null)
            {
                query = "SELECT TOP 1 * FROM [SDC_AVABILL_INVOICE_INFORMATION] WITH(NOLOCK) WHERE  INTERNAL_DATA IS NOT NULL AND RTRIM(LTRIM(INTERNAL_DATA))<>'' AND ID =@INVOICE_NUMBER;";
                // If normal sales then change query
                if (Sale != false)
                {
                    query = "SELECT TOP 1 * FROM [SDC_AVABILL_INVOICE_INFORMATION] WITH(NOLOCK) WHERE  (INTERNAL_DATA IS NULL OR RTRIM(LTRIM(INTERNAL_DATA))='') AND ID =@INVOICE_NUMBER;";
                }
            }
            
            try
            {
                using (SqlConnection connection = new SqlConnection(getConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(query,connection))
                    {
                        if (invoiceNumber != null)
                        {
                            command.Parameters.AddWithValue("INVOICE_NUMBER",invoiceNumber);
                        }

                        SqlDataReader reader = command.ExecuteReader();
                        
                        while (reader.Read())
                        {
                          CISAvabillModel invoice = new CISAvabillModel();
                          invoice = transform(reader);
                          invoices.Add(invoice);
                        }
                     
                    }
                }
            }
            catch (Exception exception)
            {
                // Something went wrong try to log it.
                Logger.error(exception.Message);
   
            }
            return invoices;
        }

        /**
         * Pick processed invoices
         */
        public static List<CISAvabillModel> pickProcessedInvoice(string invoiceNumber = null)
        {
            List<CISAvabillModel> invoices = new List<CISAvabillModel>();

            try
            {
                using (SqlConnection connection = new SqlConnection(getConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(
                    "SELECT  * FROM [SDC].[dbo].[SDC_AVABILL_INVOICE_INFORMATION] WITH(NOLOCK) WHERE INTERNAL_DATA IS NOT NULL;",
                    connection))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            CISAvabillModel invoice = new CISAvabillModel();
                            invoice = transform(reader);
                            invoices.Add(invoice);
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                // Something went wrong try to log it.
                Logger.error(exception.Message);

            }
            return invoices;
        }


        /**
         * Transform result to the Model Object
         */
        public static CISAvabillModel transform(SqlDataReader reader)
        {
            CISAvabillModel invoice = new CISAvabillModel();

            invoice.ID = reader["ID"].ToString();
            invoice.ACCOUNT_NUMBER = reader["ACCOUNT_NUMBER"].ToString();
            invoice.NORMAL_SALE_REFERENCE_NUMBER = invoice.ID;
            invoice.ACCOUNT_NAME = reader["ACCOUNT_NAME"].ToString();
            invoice.CUSTOMER_ADDRESS = reader["CUSTOMER_ADDRESS"].ToString();
            invoice.TYPE_OF_ACCOUNT = reader["TYPE_OF_ACCOUNT"].ToString();
            invoice.PAYMENT_DUE_DATE = reader["PAYMENT_DUE_DATE"].ToString();
            invoice.INVOICE_NUMBER = reader["INVOICE_NUMBER"].ToString();
            invoice.Balance_Brought_Forward = reader["Balance_Brought_Forward"].ToString();
            invoice.Adjustments = reader["Adjustments"].ToString();
            invoice.Payments_Received = reader["Payments_Received"].ToString();
            invoice.TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS = reader["TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS"].ToString();
            invoice.MONTHLY_CHARGES = reader["MONTHLY_CHARGES"].ToString();
            invoice.Total_Call_Entitlement_Charges = reader["Total_Call_Entitlement_Charges"].ToString();
            invoice.CUG_and_Feature_Charges = reader["CUG_and_Feature_Charges"].ToString();
            invoice.Printed_Bill = reader["Printed_Bill"].ToString();
            invoice.Late_Charges = reader["Late_Charges"].ToString();
            invoice.Other_Charges = reader["Other_Charges"].ToString();
            invoice.Discounts = reader["Discounts"].ToString();
            invoice.TOTAL_CHARGES_FOR_THE_MONTH = reader["TOTAL_CHARGES_FOR_THE_MONTH"].ToString();
            invoice.TOTAL_AMOUNT_DUE = reader["TOTAL_AMOUNT_DUE"].ToString();
            invoice.Total_Additional_Recharges = reader["Total_Additional_Recharges"].ToString();
            invoice.Voice_Local = reader["Voice_Local"].ToString();
            invoice.SMS = reader["SMS"].ToString();
            invoice.DATA = reader["DATA"].ToString();
            invoice.VAS = reader["VAS"].ToString();
            invoice.Voice_International = reader["Voice_International"].ToString();
            invoice.Roaming = reader["Roaming"].ToString();
            invoice.TOTAL = reader["TOTAL"].ToString();
            invoice.TOTAL_B_18 = reader["TOTAL_B_18"].ToString();
            invoice.TOTAL_TAX_B = reader["TOTAL_TAX_B"].ToString();
            invoice.TOTAL_TAX = reader["TOTAL_TAX"].ToString();
            invoice.CASH = reader["CASH"].ToString();
            invoice.ITEMS_NUMBER = reader["ITEMS_NUMBER"].ToString();
            invoice.SDC_DATE = reader["SDC_DATE"].ToString();
            invoice.SDC_TIME = reader["SDC_TIME"].ToString();
            invoice.SDC_ID = reader["SDC_ID"].ToString();
            invoice.SDC_RECEIPT_NUMBER = reader["SDC_RECEIPT_NUMBER"].ToString();
            invoice.INTERNAL_DATA = reader["INTERNAL_DATA"].ToString();
            invoice.RECEIPT_SIGNATURE = reader["RECEIPT_SIGNATURE"].ToString();
            invoice.CIS_DATE = DateTime.Now.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
            invoice.CIS_TIME = DateTime.Now.ToString("HH:mm:ss", CultureInfo.InvariantCulture); ;
            invoice.RECEIPT_TYPE_MRC = reader["RECEIPT_TYPE_MRC"].ToString();
            invoice.TIN = reader["TIN"].ToString();
            invoice.RECEIPT_NUMBER = reader["RECEIPT_NUMBER"].ToString();
            invoice.TAXRATE1 = reader["TAXRATE1"].ToString();
            invoice.TAXRRATE2 = reader["TAXRRATE2"].ToString();
            invoice.TAXRATE3 = reader["TAXRATE3"].ToString();
            invoice.TAXRATE4 = reader["TAXRATE4"].ToString();
            invoice.AMOUNT1 = reader["AMOUNT1"].ToString();
            invoice.AMOUNT2 = reader["AMOUNT2"].ToString();
            invoice.AMOUNT3 = reader["AMOUNT3"].ToString();
            invoice.AMOUNT4 = reader["AMOUNT4"].ToString();
            invoice.TAX1 = reader["TAX1"].ToString();
            invoice.TAX2 = reader["TAX2"].ToString();
            invoice.TAX3 = reader["TAX3"].ToString();
            invoice.TAX4 = reader["TAX4"].ToString();
            invoice.MRC = reader["MRC"].ToString();
            invoice.STATUS = reader["STATUS"].ToString();
            invoice.RESULTS = reader["RESULTS"].ToString();
            invoice.CREATED_AT = reader["CREATED_AT"].ToString();
            invoice.UPDATED_D_AT = reader["UPDATED_D_AT"].ToString();

            return invoice;
        }

        /**
         * Inserting into CISAVABILL MODEL 
         */
        public static void insertIntoCISAvabill(CISAvabillModel invoice)
        {
            string query ="INSERT INTO [SDC_AVABILL_INVOICE_INFORMATION]([ACCOUNT_NUMBER],[ACCOUNT_NAME],[CUSTOMER_ADDRESS],[TYPE_OF_ACCOUNT],[PAYMENT_DUE_DATE],[INVOICE_NUMBER],[Balance_Brought_Forward],[Adjustments],[Payments_Received],[TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS],[MONTHLY_CHARGES],[Total_Call_Entitlement_Charges],[CUG_and_Feature_Charges],[Printed_Bill],[Late_Charges],[Other_Charges],[Discounts],[TOTAL_CHARGES_FOR_THE_MONTH],[TOTAL_AMOUNT_DUE],[Total_Additional_Recharges],[Voice_Local],[SMS],[DATA],[VAS],[Voice_International],[Roaming],[TOTAL],[TOTAL_B_18],[TOTAL_TAX_B],[TOTAL_TAX],[CASH],[ITEMS_NUMBER],[SDC_DATE],[SDC_TIME],[SDC_ID],[SDC_RECEIPT_NUMBER],[INTERNAL_DATA],[RECEIPT_SIGNATURE],[CIS_DATE],[CIS_TIME],[RECEIPT_TYPE_MRC],[TIN],[RECEIPT_NUMBER],[TAXRATE1],[TAXRRATE2],[TAXRATE3],[TAXRATE4],[AMOUNT1],[AMOUNT2],[AMOUNT3],[AMOUNT4],[TAX1],[TAX2],[TAX3],[TAX4],[MRC],[RESULTS],[CREATED_AT],[UPDATED_D_AT],[STATUS])VALUES(@ACCOUNT_NUMBER,@ACCOUNT_NAME,@CUSTOMER_ADDRESS,@TYPE_OF_ACCOUNT,@PAYMENT_DUE_DATE,@INVOICE_NUMBER,@Balance_Brought_Forward,@Adjustments,@Payments_Received,@TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS,@MONTHLY_CHARGES,@Total_Call_Entitlement_Charges,@CUG_and_Feature_Charges,@Printed_Bill,@Late_Charges,@Other_Charges,@Discounts,@TOTAL_CHARGES_FOR_THE_MONTH,@TOTAL_AMOUNT_DUE,@Total_Additional_Recharges,@Voice_Local,@SMS,@DATA,@VAS,@Voice_International,@Roaming,@TOTAL,@TOTAL_B_18,@TOTAL_TAX_B,@TOTAL_TAX,@CASH,@ITEMS_NUMBER,@SDC_DATE,@SDC_TIME,@SDC_ID,@SDC_RECEIPT_NUMBER,@INTERNAL_DATA,@RECEIPT_SIGNATURE,@CIS_DATE,@CIS_TIME,@RECEIPT_TYPE_MRC,@TIN,@RECEIPT_NUMBER,@TAXRATE1,@TAXRRATE2,@TAXRATE3,@TAXRATE4,@AMOUNT1,@AMOUNT2,@AMOUNT3,@AMOUNT4,@TAX1,@TAX2,@TAX3,@TAX4,@MRC,@RESULTS,@CREATED_AT,@UPDATED_D_AT,@STATUS)";
            using (SqlConnection connection = new SqlConnection(getConnectionString()))
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    command.Parameters.AddWithValue("ACCOUNT_NUMBER", invoice.ACCOUNT_NUMBER);
                    command.Parameters.AddWithValue("ACCOUNT_NAME", invoice.ACCOUNT_NAME);
                    command.Parameters.AddWithValue("CUSTOMER_ADDRESS", invoice.CUSTOMER_ADDRESS);
                    command.Parameters.AddWithValue("TYPE_OF_ACCOUNT", invoice.TYPE_OF_ACCOUNT);
                    command.Parameters.AddWithValue("PAYMENT_DUE_DATE", invoice.PAYMENT_DUE_DATE);
                    command.Parameters.AddWithValue("INVOICE_NUMBER", invoice.INVOICE_NUMBER);
                    command.Parameters.AddWithValue("Balance_Brought_Forward", invoice.Balance_Brought_Forward);
                    command.Parameters.AddWithValue("Adjustments", invoice.Adjustments);
                    command.Parameters.AddWithValue("Payments_Received", invoice.Payments_Received);
                    command.Parameters.AddWithValue("TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS", invoice.TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS);
                    command.Parameters.AddWithValue("MONTHLY_CHARGES", invoice.MONTHLY_CHARGES);
                    command.Parameters.AddWithValue("Total_Call_Entitlement_Charges", invoice.Total_Call_Entitlement_Charges);
                    command.Parameters.AddWithValue("CUG_and_Feature_Charges", invoice.CUG_and_Feature_Charges);
                    command.Parameters.AddWithValue("Printed_Bill", invoice.Printed_Bill);
                    command.Parameters.AddWithValue("Late_Charges", invoice.Late_Charges);
                    command.Parameters.AddWithValue("Other_Charges", invoice.Other_Charges);
                    command.Parameters.AddWithValue("Discounts", invoice.Discounts);
                    command.Parameters.AddWithValue("TOTAL_CHARGES_FOR_THE_MONTH", invoice.TOTAL_CHARGES_FOR_THE_MONTH);
                    command.Parameters.AddWithValue("TOTAL_AMOUNT_DUE", invoice.TOTAL_AMOUNT_DUE);
                    command.Parameters.AddWithValue("Total_Additional_Recharges", invoice.Total_Additional_Recharges);
                    command.Parameters.AddWithValue("Voice_Local", invoice.Voice_Local);
                    command.Parameters.AddWithValue("SMS", invoice.SMS);
                    command.Parameters.AddWithValue("DATA", invoice.DATA);
                    command.Parameters.AddWithValue("VAS", invoice.VAS);
                    command.Parameters.AddWithValue("Voice_International", invoice.Voice_International);
                    command.Parameters.AddWithValue("Roaming", invoice.Roaming);
                    command.Parameters.AddWithValue("TOTAL", invoice.TOTAL);
                    command.Parameters.AddWithValue("TOTAL_B_18", invoice.TOTAL_B_18);
                    command.Parameters.AddWithValue("TOTAL_TAX_B", invoice.TOTAL_TAX_B);
                    command.Parameters.AddWithValue("TOTAL_TAX", invoice.TOTAL_TAX);
                    command.Parameters.AddWithValue("CASH", invoice.CASH);
                    command.Parameters.AddWithValue("ITEMS_NUMBER", invoice.ITEMS_NUMBER);
                    command.Parameters.AddWithValue("SDC_DATE", invoice.SDC_DATE);
                    command.Parameters.AddWithValue("SDC_TIME", invoice.SDC_TIME);
                    command.Parameters.AddWithValue("SDC_ID", invoice.SDC_ID);
                    command.Parameters.AddWithValue("SDC_RECEIPT_NUMBER", invoice.SDC_RECEIPT_NUMBER);
                    command.Parameters.AddWithValue("INTERNAL_DATA", invoice.INTERNAL_DATA);
                    command.Parameters.AddWithValue("RECEIPT_SIGNATURE", invoice.RECEIPT_SIGNATURE);
                    command.Parameters.AddWithValue("CIS_DATE", invoice.CIS_DATE);
                    command.Parameters.AddWithValue("CIS_TIME", invoice.CIS_TIME);
                    command.Parameters.AddWithValue("RECEIPT_TYPE_MRC", invoice.RECEIPT_TYPE_MRC);
                    command.Parameters.AddWithValue("TIN", invoice.TIN);
                    command.Parameters.AddWithValue("RECEIPT_NUMBER", invoice.RECEIPT_NUMBER);
                    command.Parameters.AddWithValue("TAXRATE1", invoice.TAXRATE1);
                    command.Parameters.AddWithValue("TAXRRATE2", invoice.TAXRRATE2);
                    command.Parameters.AddWithValue("TAXRATE3", invoice.TAXRATE3);
                    command.Parameters.AddWithValue("TAXRATE4", invoice.TAXRATE4);
                    command.Parameters.AddWithValue("AMOUNT1", invoice.AMOUNT1);
                    command.Parameters.AddWithValue("AMOUNT2", invoice.AMOUNT2);
                    command.Parameters.AddWithValue("AMOUNT3", invoice.AMOUNT3);
                    command.Parameters.AddWithValue("AMOUNT4", invoice.AMOUNT4);
                    command.Parameters.AddWithValue("TAX1", invoice.TAX1);
                    command.Parameters.AddWithValue("TAX2", invoice.TAX2);
                    command.Parameters.AddWithValue("TAX3", invoice.TAX3);
                    command.Parameters.AddWithValue("TAX4", invoice.TAX4);
                    command.Parameters.AddWithValue("MRC", invoice.MRC);
                    command.Parameters.AddWithValue("RESULTS", invoice.RESULTS);
                    command.Parameters.AddWithValue("CREATED_AT", invoice.CREATED_AT);
                    command.Parameters.AddWithValue("UPDATED_D_AT", invoice.UPDATED_D_AT);
                    command.Parameters.AddWithValue("STATUS", invoice.STATUS);

                    try
                    {
                        connection.Open();

                        command.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        Logger.critical(ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
    }
}
