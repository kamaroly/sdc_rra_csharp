﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDCCommunicator.Models
{
    class PLUReport
    {
        public string Total_Call_Entitlement_Charges { set; get; }
        public string CUG_and_Feature_Charges { set; get; }
        public string Printed_Bill { set; get; }
        public string Late_Charges { set; get; }
        public string Other_Charges { set; get; }
    }
}
