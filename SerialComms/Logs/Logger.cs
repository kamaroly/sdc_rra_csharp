﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialComms.Logs
{
    class Logger
    {
        public static void info(string message)
        {
            log(message);
        }

        public static void log(string message, string level = "info")
        {

            StreamWriter sw = null;
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logs\\" + level.ToLower() + DateTime.Now.ToString("yyyy-MM-dd") + "logs.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":" + message);
                sw.Flush();
                sw.Close();
        }
        /**
        * Log success
        */
        public static void success(string message)
        {
            log(message, "SUCCESS");
        }

        /**
         * Log warning
         */
        public static void warning(string message)
        {
            log(message, "WARNING");
        }

        /**
         * Log Critical
         */
        public static void error(string message)
        {
            log(message, "ERROR");
        }

        /**
         * Log critical 
         */
        public static void critical(string message)
        {
            log(message, "CRITICAL");
        }
        /**
         * LOG WEB Requests
         */
        public static void sdcLogs(string message)
        {
            log(message, "SDC-LOG");
        }

    }
}
