﻿using SDCCommunicator.Helpers;
using SDCCommunicator.Models;
using SerialComms.Logs;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDCCommunicator.Factories
{

    class SDCFactory
    {

        static SerialPort serialPort = new SerialPort();
        static string pathName = System.Environment.CurrentDirectory;
        static INIFile inifile = new INIFile(Properties.Settings.Default.SETTINGS_FILE_PATH);

        /**
         * Construct our object
         */
        SDCFactory()
        {
            pathName =  System.IO.Directory.GetParent(pathName).FullName;
            inifile = new INIFile(pathName + Properties.Settings.Default.SETTINGS_FILE_PATH);

        }
        /**
         * Open the port
         */
        private static void openPort()
        {
            inifile = new INIFile(pathName + "\\" + Properties.Settings.Default.SETTINGS_FILE_PATH);
            if (!serialPort.IsOpen)
            {
                string portName = Convert.ToString(inifile.Read("SDC", "PORT").ToUpper());
                serialPort.PortName = portName;
                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Handshake = Handshake.None;
                serialPort.Parity = Parity.None;
                serialPort.RtsEnable = true;
                serialPort.DtrEnable = true;
                serialPort.Open();
            }
        }

        /**
        * Close serial port
        */
        private static void closePort()
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
            }
        }

        /**
        * @Author  : Kamaro Lambert 
        * @method  : to get SDC ID
        * @COMMAND : E5
       */
        public  static Dictionary<string, string> getSdcId()
        {
            Logger.sdcLogs("===============BEGIN CHECKING SDC ID=================");

            string request = getSdcRequest("", "E5", "20");
            Dictionary<string, string> response = communicateToSdc(request);

            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }

            Logger.sdcLogs("===============END CHECKING SDC ID =================");
            return response;
        }

        /**
         * @Author  : Kamaro Lambert 
         * @method  : to get SDC STATUS
         * @COMMAND : E7
        */
        public static Dictionary<string, string> getSdcStatus()
        {
            string request = getSdcRequest("", "E7", "21");

            Logger.sdcLogs("===============BEGIN CHECKING SDC STATUS=================");
            Dictionary<string, string> response = communicateToSdc(request);

            if (response["status"].ToUpper().Contains("P"))
            {
                response.Remove("status");

                string[] responseArray = response["response"].Split(',');
                response.Add("status", "success");
                response.Add("SDC serial number", responseArray[0]);
                response.Add("Firmware version", responseArray[1]);
                response.Add("Hardware revision", responseArray[2]);
                response.Add("The number of current SDC daily report", responseArray[3]);
                response.Add("Last remote audit date and time", responseArray[4]);
                response.Add("Last local audit date and time", responseArray[5]);

                response.Remove("response");

            }

            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }

            Logger.sdcLogs("===============END CHECKING SDC STATUS=================");
            return response;
        }

        /**
        * @Author  : Kamaro Lambert 
        * @method  : To request recept counters from SDC
        * @COMMAND : C9
        * @DATA    : INVOICE NUMBER
       */
        public static Dictionary<string, string> getCounters(string data)
        {
            string request = getSdcRequest(data, "C9", "27");
            Logger.sdcLogs("===============BEGIN REQUESTING COUNTERS=================");
            Logger.sdcLogs("DATA : " + data);
            Dictionary<string, string> response = communicateToSdc(request);
            if (response["status"].ToUpper().Contains("P"))
            {
                response.Remove("status");

                string[] responseArray = response["response"].Split(',');
                response.Add("status", "success");
                response.Add("Receipt number", responseArray[0]);
                response.Add("Receipt number per receipt type", responseArray[1]);
                response.Add("Total receipt number", responseArray[2]);
                response.Add("Receipt label", responseArray[3]);

                response.Remove("response");
            }

            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }

            Logger.sdcLogs("===============END REQUESTING COUNTERS=================");
            return response;
        }

        /**
         * @Author  : Kamaro Lambert 
         * @method  : this method is used to send electronic journal to RRA
         * @COMMAND : C8
         * @DATA    : INVOICE NUMBER
        */
        public static Dictionary<string, string> getSignature(string data)
        {
            Logger.sdcLogs("===============BEGIN REQUESTING SIGNATURE=================");
            Logger.sdcLogs("DATA : " + data);

            string request = getSdcRequest(data, "C8", "29");
            Dictionary<string, string> response = communicateToSdc(request);
            if (response["status"].ToUpper().Contains("P"))
            {
                response.Remove("status");

                string[] responseArray = response["response"].Split(',');
                string[] dateTime = responseArray[4].Split(' ');
                string formattedSignature = Regex.Replace(responseArray[5], ".{4}", "$0-");
                response.Add("status", "success");
                response.Add("{SDC_DATE}", dateTime[0]);
                response.Add("{SDC_TIME}", dateTime[1]);
                response.Add("{SDC_ID}", responseArray[0]);
                response.Add("{SDC_RECEIPT_NUMBER}", responseArray[1] + "/" + responseArray[2] + " " + responseArray[3]);
                response.Add("{INTERNAL_DATA}", Regex.Replace(responseArray[6], ".{4}", "$0-")); // Insert a dash every 4 characters
                response.Add("{RECEIPT_SIGNATURE}", formattedSignature.Substring(0,formattedSignature.Length - 1));// Insert a dash every 4 characters

                response.Remove("response");
            }

            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }

            Logger.sdcLogs("===============END REQUESTING SIGNATURE=================");
            return response;
        }

        /**
        * @Author : Kamaro Lambert 
        * @method : this method is used to send electronic journal to RRA
        * @COMMAND : EE
        *  Current line type:
        *  ====================
        * 'B' mark for begin of the receipt
        * 'N' mark for line into the body of receipt
        * 'E' mark for end of receipt 
        *  Read the file and display it line by line.
        */
        public static Dictionary<string, string> sendElectronicJournal(CISAvabillModel invoiceToProcess)
        {

            Logger.sdcLogs("===============BEGIN SENDING ELECTORNIC JOURNAL=================");
            int counter = 0;
            string line;
            string request;
            int sequenceInt = 32;
            string sequence;
            string lineType = "B";

            Dictionary<string, string> response = new Dictionary<string, string>();

            // GENERATE DUMPS INFORMATION TO BE USED
            Dictionary<string, string> invoiceInformation = new Dictionary<string, string>();

            // TIGO INFORMATION
            invoiceInformation.Add("{TIN}", invoiceToProcess.TIN);

            // CUSTOMER
            invoiceInformation.Add("{ACCOUNT_NUMBER}", invoiceToProcess.ACCOUNT_NUMBER);
            invoiceInformation.Add("{ACCOUNT_NAME}", invoiceToProcess.ACCOUNT_NAME);
            invoiceInformation.Add("{CUSTOMER_ADDRESS}", invoiceToProcess.CUSTOMER_ADDRESS);
            invoiceInformation.Add("{TYPE_OF_ACCOUNT}", invoiceToProcess.TYPE_OF_ACCOUNT);
            invoiceInformation.Add("{PAYMENT_DUE_DATE}", invoiceToProcess.PAYMENT_DUE_DATE);

            // IF THIS IS A NORMAL REFUND ADD PREVIOUS INVOICE NUMBER
            if (invoiceToProcess.RECEIPT_TYPE_MRC.Trim().Equals("NR"))
            {
                invoiceToProcess.INVOICE_NUMBER += "\n";
                invoiceToProcess.INVOICE_NUMBER += "NORMAL SALE REFERENCE #:" + invoiceToProcess.NORMAL_SALE_REFERENCE_NUMBER;
            }
            // INVOICE DETAILS
            invoiceInformation.Add("{INVOICE_NUMBER}", invoiceToProcess.INVOICE_NUMBER);
            invoiceInformation.Add("{Balance_Brought_Forward}", invoiceToProcess.Balance_Brought_Forward);
            invoiceInformation.Add("{Adjustments}", invoiceToProcess.Adjustments);
            invoiceInformation.Add("{Payments_Received}", invoiceToProcess.Payments_Received);
            invoiceInformation.Add("{TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS}", invoiceToProcess.TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS);
            invoiceInformation.Add("{MONTHLY_CHARGES}", invoiceToProcess.MONTHLY_CHARGES);
            invoiceInformation.Add("{Total_Call_Entitlement_Charges}", invoiceToProcess.Total_Call_Entitlement_Charges);
            invoiceInformation.Add("{CUG_and_Feature_Charges}", invoiceToProcess.CUG_and_Feature_Charges);
            invoiceInformation.Add("{Printed_Bill}", invoiceToProcess.Printed_Bill);
            invoiceInformation.Add("{Late_Charges}", invoiceToProcess.Late_Charges);
            invoiceInformation.Add("{Other_Charges}", invoiceToProcess.Other_Charges);
            invoiceInformation.Add("{Discounts}", invoiceToProcess.Discounts);
            invoiceInformation.Add("{TOTAL_CHARGES_FOR_THE_MONTH}", invoiceToProcess.TOTAL_CHARGES_FOR_THE_MONTH);
            invoiceInformation.Add("{TOTAL_AMOUNT_DUE}", invoiceToProcess.TOTAL_AMOUNT_DUE);
            invoiceInformation.Add("{Total_Additional_Recharges}", invoiceToProcess.Total_Additional_Recharges);
            invoiceInformation.Add("{Voice_Local}", invoiceToProcess.Voice_Local);
            invoiceInformation.Add("{SMS}", invoiceToProcess.SMS);
            invoiceInformation.Add("{DATA}", invoiceToProcess.DATA);
            invoiceInformation.Add("{VAS}", invoiceToProcess.VAS);
            invoiceInformation.Add("{Voice_International}", invoiceToProcess.Voice_International);
            invoiceInformation.Add("{Roaming}", invoiceToProcess.Roaming);

            // AMOUNT INFORMATION
            invoiceInformation.Add("{TOTAL}", invoiceToProcess.TOTAL);
            invoiceInformation.Add("{TOTAL_B_18}", invoiceToProcess.TOTAL_B_18);
            invoiceInformation.Add("{TOTAL_TAX_B}", invoiceToProcess.TOTAL_TAX_B);
            invoiceInformation.Add("{TOTAL_TAX}", invoiceToProcess.TOTAL_TAX);

            // PAYMENT METHOD
            invoiceInformation.Add("{CASH}", invoiceToProcess.CASH);
            invoiceInformation.Add("{ITEMS_NUMBER}", invoiceToProcess.ITEMS_NUMBER);

            // SDC INFORMATION
            invoiceInformation.Add("{SDC_DATE}", invoiceToProcess.SDC_DATE);
            invoiceInformation.Add("{SDC_TIME}", invoiceToProcess.SDC_TIME);
            invoiceInformation.Add("{SDC_ID}", invoiceToProcess.SDC_ID);
            invoiceInformation.Add("{SDC_RECEIPT_NUMBER}", invoiceToProcess.SDC_RECEIPT_NUMBER);
            invoiceInformation.Add("{INTERNAL_DATA}", invoiceToProcess.INTERNAL_DATA);
            invoiceInformation.Add("{RECEIPT_SIGNATURE}", invoiceToProcess.RECEIPT_SIGNATURE);
                
            // CIS INFORMATION
            invoiceInformation.Add("{CIS_RECEIPT_NUMBER}", invoiceToProcess.ID);
            invoiceInformation.Add("{CIS_DATE}", invoiceToProcess.CIS_DATE);
            invoiceInformation.Add("{CIS_TIME}", invoiceToProcess.CIS_TIME);
            invoiceInformation.Add("{MRC}", invoiceToProcess.MRC);

            // Current line type:
            //'B' mark for begin of the receipt
            //'N' mark for line into the body of receipt
            //'E' mark for end of receipt 
            // Read the file and display it line by line.
           
            sequence = sequenceInt.ToString("X");
            request = getSdcRequest(lineType, "EE", sequence);
            response = communicateToSdc(request);
            sequenceInt += 1;
            Logger.sdcLogs(lineType);
            string template = @"InvoiceTemplates\" + "A4" + ".txt";
            System.IO.StreamReader file = new System.IO.StreamReader(template);
            string invoice = file.ReadToEnd();
            
            foreach (KeyValuePair<string, string> info in invoiceInformation)
            {
                invoice = invoice.Replace(info.Key, info.Value);
            }
            // WriteAllText creates a file, writes the specified string to the file,
            // and then closes the file.    You do NOT need to call Flush() or Close().
        

            // SAVE GENERATED INVOICE
            string invoiceName = "INVOICE_" + invoiceInformation["{ACCOUNT_NUMBER}"] +"-"+ invoiceInformation["{CIS_DATE}"].Replace("/","") + ".txt";
            template = @"InvoiceTemplates\" + invoiceName;
            System.IO.File.WriteAllText(template, invoice);

            // READ GENERATED INVOICE
            file = new System.IO.StreamReader(template);

            while ((line = file.ReadLine()) != null)
            {
                if (Control.ModifierKeys == Keys.Escape)//it should stop now
                {
                    MessageBox.Show("You have pressed Escape Key, we are stopping execution.");
                    return response;
                }

                if (sequenceInt > 127)
                {
                    sequenceInt = 32;
                }

                line = "N" + line;
                Logger.sdcLogs(line);
                sequence = sequenceInt.ToString("X");
                request = getSdcRequest(line, "EE", sequence);
                line = counter.ToString() + line;
                response.Add(line, communicateToSdc(request)["response"].ToString());

                counter++;
                sequenceInt += 1;
            }
            lineType = "E";
            sequence = sequenceInt.ToString("X");
            request = getSdcRequest(lineType, "EE", sequence);
            response = communicateToSdc(request);
            sequenceInt += 1;
            Logger.sdcLogs(lineType);
            file.Close();

            (new PrintInvoice()).pdf(invoiceName);
            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }
            Logger.sdcLogs("===============END OF SENDING ELECTORNIC JOURNAL=================");

            response.Add("Electronic_journal",invoice);
            return response;
        }

        /**
         * @Author  : Kamaro Lambert 
         * @method  : this method is used to send electronic journal to RRA
         * @COMMAND : C6
         * @DATA    : INVOICE DETAILS (RtypeTTypeMRC,TIN,Date TIME, Rnumber,TaxRate1,TaxrRate2,TaxRate3,TaxRate4,Amount1,Amount2,Amount3,Amount4,Tax1,Tax2,Tax3,Tax4)
         * @EXAMPLE : nstes01012345,100600570,17/07/2013 09:29:37,1,0.00,18.00,0.00,0.00,11.00,12.00,0.00,0.00,0.00,1.83,0.00,0.00
        */
        public static Dictionary<string, string> sendReceiptData(string data)
        {

            Logger.sdcLogs("===============BEGIN SENDING RECEIPT DATA=================");
            Logger.sdcLogs("DATA : " + data);
            string request = getSdcRequest(data, "C6", "23");
            Dictionary<string, string> response = communicateToSdc(request);

            // Log clean response
            foreach (KeyValuePair<string, string> element in response)
            {
                Logger.sdcLogs(element.Key.ToUpper() + " : " + element.Value);
            }
            Logger.sdcLogs("===============END SENDING RECEIPT DATA=================");

            return response;
        }
        /**
        * Get SDC request method
        * @Author Kamaro Lambert
        * @param string data //  RtypeTTypeMRC,TIN,Date TIME, Rnumber,TaxRate1,TaxrRate2,TaxRate3,TaxRate4,Amount1,Amount2,Amount3,Amount4,Tax1,Tax2,Tax3,Tax4
        *                    // Example : "nstes01012345,100600570,17/07/2013 09:29:37,1,0.00,18.00,0.00,0.00,11.00,12.00,0.00,0.00,0.00,1.83,0.00,0.00"
        * @param string command // Command to sdc example c6
        */
        private static string getSdcRequest(string data, string command, string sequence)
        {
            string commandLength;   // Length of the command
            string commandBcc;      // CheckSum of the command
            string request;

            // Make sure ALL are in  caps
            data = data.ToUpper();
            data = getHexData(data);
            command = command.ToUpper();
            request = sequence + " " + command.ToUpper() + " 05";

            // if the data is not empty then add it to the command
            if (string.IsNullOrWhiteSpace(data) == false)
            {
                request = sequence + " " + command.ToUpper() + " " + data + " 05";
            }

            // Get the length of the byte hex to be sent
            commandLength = getLength(data);

            // Add length to the request
            request = commandLength + " " + request;

            // Get checksum(BCC) of this command
            commandBcc = getBcc(request);

            // For example to look for serial number you have to pass "01 24 20 E5 05 30 31 32 3E 03" OR 
            // SDC Status "01 24 20 E7 05 30 31 33 30 03"
            return request = "01" + " " + request + " " + commandBcc + " 03";
        }
        
        /**
         * Bill the invoices
         */
        public static string bill(CISAvabillModel invoice)
        {
            invoice.RECEIPT_NUMBER = invoice.ID;

            inifile = new INIFile(pathName + "\\" + Properties.Settings.Default.SETTINGS_FILE_PATH);
            invoice.TIN = inifile.Read("SDC", "TIN");
            invoice.MRC = inifile.Read("SDC", "MRC");
            // 1. Check if SDC is available
            Dictionary<string, string> finalResponse = new Dictionary<string, string>();
            Dictionary<string, string> response = new Dictionary<string, string>();
            // 2. SEND receipt DATA
            finalResponse = new Dictionary<string, string>();
            response = SDCFactory.getSdcId();
            finalResponse = finalResponse.Concat(response).ToDictionary(element => element.Key, element => element.Value);

            string command = invoice.RECEIPT_TYPE_MRC + invoice.MRC + "," +
                                  invoice.TIN + "," + invoice.CIS_DATE + " " +
                                  invoice.CIS_TIME + "," + invoice.ID + "," +
                                  invoice.TAXRATE1 + "," + invoice.TAXRRATE2 + "," + invoice.TAXRATE3 + "," + invoice.TAXRATE4 + "," +
                                  invoice.AMOUNT1 + "," + invoice.AMOUNT2 + "," + invoice.AMOUNT3 + "," + invoice.AMOUNT4 + "," +
                                  invoice.TAX1 + "," + invoice.TAX2 + "," + invoice.TAX3 + "," + invoice.TAX4;

            response = SDCFactory.sendReceiptData(command);
            // Don't continue if we have an error
            if (response["status"].ToUpper().Trim().StartsWith("E"))
            {
                return response["response"];
            }

            response.Remove("status");
            response.Remove("response");

            finalResponse = finalResponse.Concat(response).ToDictionary(element => element.Key, element => element.Value);

            // 3. GET  SIGNATURE
            response = SDCFactory.getSignature(invoice.RECEIPT_NUMBER);
            // Don't continue if we have an error
            if (response["status"].ToLower().Equals("error"))
            {
                return response["response"];
            }
            response.Remove("status");
            response.Remove("response");

            finalResponse = finalResponse.Concat(response).ToDictionary(element => element.Key, element => element.Value);

            // 4. Build Receipt     
            invoice.SDC_DATE = finalResponse["{SDC_DATE}"];
            invoice.SDC_TIME = finalResponse["{SDC_TIME}"];
            invoice.SDC_ID = finalResponse["{SDC_ID}"];
            invoice.SDC_RECEIPT_NUMBER = finalResponse["{SDC_RECEIPT_NUMBER}"];
            invoice.INTERNAL_DATA = finalResponse["{INTERNAL_DATA}"];
            invoice.RECEIPT_SIGNATURE = finalResponse["{RECEIPT_SIGNATURE}"];

            // CONVERT POSITIVE VALUES TO NEGATIVE
            invoice = convertNStoNR(invoice);

            // 5. BUILD PDF AND SAVE IT.
            response = SDCFactory.sendElectronicJournal(invoice);

            invoice.RESULTS = response["Electronic_journal"];
            invoice.STATUS = response["status"];

            // SAVE IN THE DATABASE
            if (invoice.RECEIPT_TYPE_MRC.Trim().Equals("NR"))
            {
                CISAvabillModel.insertIntoCISAvabill(invoice);
            }
            else
            {
                CISAvabillModel.updateInvoice(invoice);
            }

            // Don't continue if we have an error
            if (response["status"].ToLower().Equals("error"))
            {
                return response["response"];
            }

            return "success";
        }

        // CONVERT NORMAL SALE INVOICE TO NORMAL RETURN BY ADDING NEGATIVE SIGN
        private static CISAvabillModel convertNStoNR(CISAvabillModel invoice)
        {
            if (invoice.RECEIPT_TYPE_MRC.Trim().Equals("NR"))
            {
                invoice.Balance_Brought_Forward = "-" + invoice.Balance_Brought_Forward;
                invoice.MONTHLY_CHARGES = "-" + invoice.MONTHLY_CHARGES;
                invoice.Total_Call_Entitlement_Charges = "-" + invoice.Total_Call_Entitlement_Charges;
                invoice.CUG_and_Feature_Charges = "-" + invoice.CUG_and_Feature_Charges;
                invoice.Printed_Bill = "-" + invoice.Printed_Bill;
                invoice.Late_Charges = "-" + invoice.Late_Charges;
                invoice.Other_Charges = "-" + invoice.Other_Charges;
                invoice.Discounts = "-" + invoice.Discounts;
                invoice.TOTAL_CHARGES_FOR_THE_MONTH = "-" + invoice.TOTAL_CHARGES_FOR_THE_MONTH;
                invoice.Total_Additional_Recharges = "-" + invoice.Total_Additional_Recharges;
                invoice.Voice_Local = "-" + invoice.Voice_Local;
                invoice.SMS = "-" + invoice.SMS;
                invoice.DATA = "-" + invoice.DATA;
                invoice.VAS = "-" + invoice.VAS;
                invoice.Voice_International = "-" + invoice.Voice_International;
                invoice.Roaming = "-" + invoice.Roaming;
                invoice.TOTAL = "-" + invoice.TOTAL;
                invoice.TOTAL_B_18 = "-" + invoice.TOTAL_B_18;
                invoice.TOTAL_TAX_B = "-" + invoice.TOTAL_TAX_B;
                invoice.TOTAL_TAX = "-" + invoice.TOTAL_TAX;
                invoice.CASH = "-" + invoice.CASH;
                invoice.ITEMS_NUMBER = "-" + invoice.ITEMS_NUMBER;
            }
            return invoice;
        }

        /**
         * Method to send request to SDC
         */
        public static Dictionary<string, string> communicateToSdc(string commandString)
        {

            Logger.sdcLogs("COMMAND STRING:" + commandString);
            Dictionary<string, string> cleanResponse = new Dictionary<string, string>();
            try
            {
                if (Control.ModifierKeys == Keys.Escape)//it should stop now
                {
                    MessageBox.Show("You have pressed Escape Key, we are stopping execution");
                    return cleanResponse;
                }

                openPort();
                // Writing bytes to serial port
                byte[] bytes = commandString.Split(' ').Select(s => Convert.ToByte(s, 16)).ToArray();
                serialPort.Write(bytes, 0, bytes.Length);

                // Wait 1 millsecond for the answer
                System.Threading.Thread.Sleep(Properties.Settings.Default.SERIAL_COMMUNICATION_SLEEPING_TIME);

                // Read response from the serial ports
                int responsebytes = serialPort.BytesToRead;
                byte[] buffer = new byte[responsebytes];
                serialPort.Read(buffer, 0, responsebytes);

                // returns the position of end of data <01><LEN><SEQ><CMD><DATA><04><STATUS><05><BCC><03>
                // Get position of 4
                int beginOfStatus = Array.IndexOf(buffer, (byte)4);

                // Get position of 5
                int endOfStatus = Array.IndexOf(buffer, (byte)5) - beginOfStatus - 1;

                byte[] statusBytes = new ArraySegment<byte>(buffer, beginOfStatus + 1, endOfStatus).ToArray();

                string status = System.Text.Encoding.UTF8.GetString(statusBytes, 0, statusBytes.Length);
                string response;

                if (status.ToUpper().Contains("P"))
                {
                    // Remove 4 the status part of the response, so that we can have it clean
                    beginOfStatus -= 4;
                    byte[] cleanData = new ArraySegment<byte>(buffer, 4, beginOfStatus).ToArray();
                    cleanData = Decode(cleanData);
                    response = System.Text.Encoding.UTF8.GetString(cleanData, 0, beginOfStatus);
                }
                else
                {
                    response = getErrorMessage(status);
                }

                closePort();

                cleanResponse.Add("status", status);
                cleanResponse.Add("response", response);

                Logger.sdcLogs("RESPONSE STATUS : " + status);
                Logger.sdcLogs("RESPONSE  : " + response);
                return cleanResponse;
            }
            catch (Exception exception)
            {
                string message = exception.Message;
                if (exception.Message.Contains("does not exist."))
                {
                    message = "Unable to detect SDC on the provided PORT, Please check if SDC is connected.(" + exception.Message+")";
                }

                cleanResponse.Add("status", "ERROR");
                cleanResponse.Add("response", message);

                Logger.sdcLogs("RESPONSE STATUS : ERROR");
                Logger.sdcLogs("RESPONSE  : " + message);

                return cleanResponse;
            }
        }

        /**
        * Method to generate command to be sent to SDC
        */
        private static string getHexData(string dataString)
        {
            // First you'll need to get it into a byte[], so do this:
            byte[] bytes = Encoding.Default.GetBytes(dataString);

            //and then you can get the string:
            string hexString = BitConverter.ToString(bytes);

            // now, that's going to return a string with dashes (-) in it so you can then simply use this:
            return hexString.Replace("-", " ").ToUpper();
        }

        /**
         * Number of bytes from <01> (excluded) to <05> (included) plus a fixed offset
         * 20h Length: 1 byte;
         * 
         * @Author Lambert Kamaro
         */
        private static string getLength(string data)
        {
            // Find the length by counting the data and adding length itsself, 
            // sequence,command,Post amble 05 (TOTAL=4)
            //, and 20h which is 32 in decimal which is 36 in total
            int length = 36;

            if (!string.IsNullOrWhiteSpace(data))
            {
                // Make sure that data is in capital letter 
                data = data.ToUpper();
                byte[] bytes = data.Split(' ').Select(s => Convert.ToByte(s, 16)).ToArray();
                length += bytes.Length;
            }

            byte[] lengthArray = BitConverter.GetBytes(length);
            lengthArray = Decode(lengthArray);
            string hex = BitConverter.ToString(lengthArray);

            return hex.ToUpper();
        }

        /**
        * @Author Kamaro Lambert
        * Method to get the BCC or the HEX to send to SDC
        * ===============================================
        * Check sum (0000h-FFFFh)
        * Length: 4 bytes; value: 30h - 3Fh
        * The check sum is formed by the bytes following <01> (without it) to <05>
        * included, by summing (adding) the values of the bytes. Each digit is sent as
        * an ASCII code.
        * =============================================== 
        * @param string $string sum of hex bytes between 01 excluded and 05 included
        * @return string
        */
        private static string getBcc(string dataWithLengthAndCommand)
        {
            // First make sure the string is in capital
            //dataWithLengthAndCommand = dataWithLengthAndCommand.ToUpper();
            string[] dataArray = dataWithLengthAndCommand.Split(' ');
            int checkSum = 0; // This will hold the sum of values of the bytes
            foreach (string hexBit in dataArray)
            {
                int asciiValue = Convert.ToInt32(hexBit, 16);
                checkSum += asciiValue;
            }

            string checkHex = string.Format("{0:X2}", checkSum);
            // Use ToCharArray to convert string to array.
            char[] array = checkHex.ToCharArray();
            int hexArraySize = (checkHex.Length - 4) * -1;
            string[] checkSumString = new string[hexArraySize + checkHex.Length];

            // Fill empty bits with 30 as 30 is the minimum value
            int index = hexArraySize;
            hexArraySize = hexArraySize - 1;
            while (hexArraySize >= 0)
            {
                checkSumString[hexArraySize] = "30";
                hexArraySize--;
            }

            // Fill the rest of the array.
            for (int i = 0; i < array.Length; i++)
            {
                // Get character from array.
                checkSumString[index++] = "3" + array[i].ToString();
            }
            // Conver the checkSum to the it's corresponding hex value
            string checkSumBcc = string.Join(" ", checkSumString);

            return checkSumBcc.ToUpper();
        }

        /**
         * Remove 00 data from this array
         */
        public static byte[] Decode(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }

        /**
         * Get error message
         */
        private static string getErrorMessage(string status)
        {
            if (status.StartsWith("E00"))
            {
                return "Error:00 no error";
            }
            if (status.StartsWith("E11"))
            {
                return "Error:11 internal memory full";
            }
            if (status.StartsWith("E12"))
            {
                return "Error:12 internal data corrupted";
            }
            if (status.StartsWith("E13"))
            {
                return "Error:13 internal memory error";
            }
            if (status.StartsWith("E20"))
            {
                return "Error:20 Real Time Clock error";
            }
            if (status.StartsWith("E30"))
            {
                return "Error:30 wrong command code";
            }
            if (status.StartsWith("E31"))
            {
                return "Error:31 wrong data format in the CIS request data";
            }
            if (status.StartsWith("E32"))
            {
                return "Error:32 wrong TIN in the CIS request data";
            }
            if (status.StartsWith("E33"))
            {
                return "Error:33 wrong tax rate in the CIS request data";
            }
            if (status.StartsWith("E34"))
            {
                return "Error:34 invalid receipt number int the CIS request data";
            }
            if (status.StartsWith("E40"))
            {
                return "Error:40 SDC not activated";
            }
            if (status.StartsWith("E41"))
            {
                return "Error:41 SDC already activated";
            }
            if (status.StartsWith("E90"))
            {
                return "Error:90 SIM card error";
            }
            if (status.StartsWith("E91"))
            {
                return "Error:91 GPRS modem error";
            }
            if (status.StartsWith("E99"))
            {
                return "Error:99 hardware intervention is necessary.";
            }

            if (status.StartsWith("W1"))
            {
                return "Error:1 SDC internal memory is near to full (it is at more than 90% of capacity).";
            }

            if (status.StartsWith("W2"))
            {
                return "SDC internal memory is near to full (it is at more than 95% of capacity) .";
            }

            return "Unknow error :" + status;
        }
    }
}
