﻿using SDCCommunicator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDCCommunicator.Factories
{
    class Reports
    {
        /**
 * Report GET XZ Report
 */
        public static void getXZReport(string startDate, string endDate)
        {
            CSVExport myExport = new CSVExport();

            List<ReportItem> report = ReportModel.getZxrepot(startDate, endDate);

            if (report.Count() == 0)
            {
                MessageBox.Show("We have nothing to show");
                return;
            }

            foreach (ReportItem row in report)
            {
                myExport.AddRow();
                myExport["Name"] = row.item;
                myExport["Total"] = row.value;
            }

            /// Then you can do any of the following three output options:
            string myCsv = myExport.Export();

            byte[] myCsvData = myExport.ExportToBytes();

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.FileName = "PLUREPORT-" + startDate.Replace("/", "-") + '-' + endDate.Replace("/", "-") + ".csv";
                if (DialogResult.OK != saveFileDialog.ShowDialog())
                    return;
                File.WriteAllBytes(saveFileDialog.FileName, myCsvData);
            }
        }
        /**
        * Report GET PLU Report
        */
        public static void getPLUReport(string startDate, string endDate)
        {
            CSVExport myExport = new CSVExport();

            List<ReportItem> report = ReportModel.getPLUreport(startDate, endDate);

            if (report.Count() == 0)
            {
                MessageBox.Show("We have nothing to show");
                return;
            }

            foreach (ReportItem row in report)
            {
                myExport.AddRow();
                myExport["Name"] = row.item;
                myExport["Total"] = row.value;
            }

            /// Then you can do any of the following three output options:
            string myCsv = myExport.Export();

            byte[] myCsvData = myExport.ExportToBytes();

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.FileName = "PLUREPORT-" + startDate.Replace("/", "-") + '-' + endDate.Replace("/", "-") + ".csv";
                if (DialogResult.OK != saveFileDialog.ShowDialog())
                    return;
                File.WriteAllBytes(saveFileDialog.FileName, myCsvData);
            }
        }

        /**
         * Report unProcessed invoices
         */
        public static void getUnproccessed()
        {
            CSVExport myExport = new CSVExport();

            List<CISAvabillModel> invoices = CISAvabillModel.pickInvoices();

            if (invoices.Count() == 0)
            {
                MessageBox.Show("We have nothing to show since all invoices are proccessed");

                return;
            }

            foreach (CISAvabillModel invoice in invoices)
            {
                myExport.AddRow();
                myExport["ID"] = invoice.ID.ToString();
                myExport["ACCOUNT_NUMBER"] = invoice.ACCOUNT_NUMBER.ToString();
                myExport["ACCOUNT_NAME"] = invoice.ACCOUNT_NAME.ToString();
                myExport["CUSTOMER_ADDRESS"] = invoice.CUSTOMER_ADDRESS.ToString();
                myExport["TYPE_OF_ACCOUNT"] = invoice.TYPE_OF_ACCOUNT.ToString();
                myExport["PAYMENT_DUE_DATE"] = invoice.PAYMENT_DUE_DATE.ToString();
                myExport["INVOICE_NUMBER"] = invoice.INVOICE_NUMBER.ToString();
                myExport["Balance_Brought_Forward"] = invoice.Balance_Brought_Forward.ToString();
                myExport["Adjustments"] = invoice.Adjustments.ToString();
                myExport["Payments_Received"] = invoice.Payments_Received.ToString();
                myExport["TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS"] = invoice.TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS.ToString();
                myExport["MONTHLY_CHARGES"] = invoice.MONTHLY_CHARGES.ToString();
                myExport["Total_Call_Entitlement_Charges"] = invoice.Total_Call_Entitlement_Charges.ToString();
                myExport["CUG_and_Feature_Charges"] = invoice.CUG_and_Feature_Charges.ToString();
                myExport["Printed_Bill"] = invoice.Printed_Bill.ToString();
                myExport["Late_Charges"] = invoice.Late_Charges.ToString();
                myExport["Other_Charges"] = invoice.Other_Charges.ToString();
                myExport["Discounts"] = invoice.Discounts.ToString();
                myExport["TOTAL_CHARGES_FOR_THE_MONTH"] = invoice.TOTAL_CHARGES_FOR_THE_MONTH.ToString();
                myExport["TOTAL_AMOUNT_DUE"] = invoice.TOTAL_AMOUNT_DUE.ToString();
                myExport["Total_Additional_Recharges"] = invoice.Total_Additional_Recharges.ToString();
                myExport["Voice_Local"] = invoice.Voice_Local.ToString();
                myExport["SMS"] = invoice.SMS.ToString();
                myExport["DATA"] = invoice.DATA.ToString();
                myExport["VAS"] = invoice.VAS.ToString();
                myExport["Voice_International"] = invoice.Voice_International.ToString();
                myExport["Roaming"] = invoice.Roaming.ToString();
                myExport["TOTAL"] = invoice.TOTAL.ToString();
                myExport["TOTAL_B_18"] = invoice.TOTAL_B_18.ToString();
                myExport["TOTAL_TAX_B"] = invoice.TOTAL_TAX_B.ToString();
                myExport["TOTAL_TAX"] = invoice.TOTAL_TAX.ToString();
                myExport["CASH"] = invoice.CASH.ToString();
                myExport["ITEMS_NUMBER"] = invoice.ITEMS_NUMBER.ToString();
                myExport["SDC_DATE"] = invoice.SDC_DATE.ToString();
                myExport["SDC_TIME"] = invoice.SDC_TIME.ToString();
                myExport["SDC_ID"] = invoice.SDC_ID.ToString();
                myExport["SDC_RECEIPT_NUMBER"] = invoice.SDC_RECEIPT_NUMBER.ToString();
                myExport["INTERNAL_DATA"] = invoice.INTERNAL_DATA.ToString();
                myExport["RECEIPT_SIGNATURE"] = invoice.RECEIPT_SIGNATURE.ToString();
                myExport["CIS_DATE"] = invoice.CIS_DATE.ToString();
                myExport["CIS_TIME"] = invoice.CIS_TIME.ToString();
                myExport["RECEIPT_TYPE_MRC"] = invoice.RECEIPT_TYPE_MRC.ToString();
                myExport["TIN"] = invoice.TIN.ToString();
                myExport["RECEIPT_NUMBER"] = invoice.RECEIPT_NUMBER.ToString();
                myExport["TAXRATE1"] = invoice.TAXRATE1.ToString();
                myExport["TAXRRATE2"] = invoice.TAXRRATE2.ToString();
                myExport["TAXRATE3"] = invoice.TAXRATE3.ToString();
                myExport["TAXRATE4"] = invoice.TAXRATE4.ToString();
                myExport["AMOUNT1"] = invoice.AMOUNT1.ToString();
                myExport["AMOUNT2"] = invoice.AMOUNT2.ToString();
                myExport["AMOUNT3"] = invoice.AMOUNT3.ToString();
                myExport["AMOUNT4"] = invoice.AMOUNT4.ToString();
                myExport["TAX1"] = invoice.TAX1.ToString();
                myExport["TAX2"] = invoice.TAX2.ToString();
                myExport["TAX3"] = invoice.TAX3.ToString();
                myExport["TAX4"] = invoice.TAX4.ToString();
                myExport["MRC"] = invoice.MRC.ToString();
                myExport["STATUS"] = invoice.STATUS.ToString();
                myExport["RESULTS"] = invoice.RESULTS.ToString();
                myExport["CREATED_AT"] = invoice.CREATED_AT.ToString();
                myExport["UPDATED_D_AT"] = invoice.UPDATED_D_AT.ToString();
            }

            /// Then you can do any of the following three output options:
            string myCsv = myExport.Export();

            byte[] myCsvData = myExport.ExportToBytes();

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.FileName = "UnProccessed"+ DateTime.Now.ToString("dd-M-yyyy") + ".csv";
                if (DialogResult.OK != saveFileDialog.ShowDialog())
                    return;
                File.WriteAllBytes(saveFileDialog.FileName,myCsvData);
            }
        }

        /**
         * Report processed invoices
         */
        public static void getProccessedInvoices()
        {
            CSVExport myExport = new CSVExport();

            List<CISAvabillModel> invoices = CISAvabillModel.pickProcessedInvoice();

            if (invoices.Count() == 0)
            {
                MessageBox.Show("We have nothing to show since no invoice invoices are proccessed");
                return;
            }

            foreach (CISAvabillModel invoice in invoices)
            {
                myExport.AddRow();
                myExport["ID"] = invoice.ID.ToString();
                myExport["ACCOUNT_NUMBER"] = invoice.ACCOUNT_NUMBER.ToString();
                myExport["ACCOUNT_NAME"] = invoice.ACCOUNT_NAME.ToString();
                myExport["CUSTOMER_ADDRESS"] = invoice.CUSTOMER_ADDRESS.ToString();
                myExport["TYPE_OF_ACCOUNT"] = invoice.TYPE_OF_ACCOUNT.ToString();
                myExport["PAYMENT_DUE_DATE"] = invoice.PAYMENT_DUE_DATE.ToString();
                myExport["INVOICE_NUMBER"] = invoice.INVOICE_NUMBER.ToString();
                myExport["Balance_Brought_Forward"] = invoice.Balance_Brought_Forward.ToString();
                myExport["Adjustments"] = invoice.Adjustments.ToString();
                myExport["Payments_Received"] = invoice.Payments_Received.ToString();
                myExport["TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS"] = invoice.TOTAL_OUTSTANDING_FROM_PREVIOUS_PERIODS.ToString();
                myExport["MONTHLY_CHARGES"] = invoice.MONTHLY_CHARGES.ToString();
                myExport["Total_Call_Entitlement_Charges"] = invoice.Total_Call_Entitlement_Charges.ToString();
                myExport["CUG_and_Feature_Charges"] = invoice.CUG_and_Feature_Charges.ToString();
                myExport["Printed_Bill"] = invoice.Printed_Bill.ToString();
                myExport["Late_Charges"] = invoice.Late_Charges.ToString();
                myExport["Other_Charges"] = invoice.Other_Charges.ToString();
                myExport["Discounts"] = invoice.Discounts.ToString();
                myExport["TOTAL_CHARGES_FOR_THE_MONTH"] = invoice.TOTAL_CHARGES_FOR_THE_MONTH.ToString();
                myExport["TOTAL_AMOUNT_DUE"] = invoice.TOTAL_AMOUNT_DUE.ToString();
                myExport["Total_Additional_Recharges"] = invoice.Total_Additional_Recharges.ToString();
                myExport["Voice_Local"] = invoice.Voice_Local.ToString();
                myExport["SMS"] = invoice.SMS.ToString();
                myExport["DATA"] = invoice.DATA.ToString();
                myExport["VAS"] = invoice.VAS.ToString();
                myExport["Voice_International"] = invoice.Voice_International.ToString();
                myExport["Roaming"] = invoice.Roaming.ToString();
                myExport["TOTAL"] = invoice.TOTAL.ToString();
                myExport["TOTAL_B_18"] = invoice.TOTAL_B_18.ToString();
                myExport["TOTAL_TAX_B"] = invoice.TOTAL_TAX_B.ToString();
                myExport["TOTAL_TAX"] = invoice.TOTAL_TAX.ToString();
                myExport["CASH"] = invoice.CASH.ToString();
                myExport["ITEMS_NUMBER"] = invoice.ITEMS_NUMBER.ToString();
                myExport["SDC_DATE"] = invoice.SDC_DATE.ToString();
                myExport["SDC_TIME"] = invoice.SDC_TIME.ToString();
                myExport["SDC_ID"] = invoice.SDC_ID.ToString();
                myExport["SDC_RECEIPT_NUMBER"] = invoice.SDC_RECEIPT_NUMBER.ToString();
                myExport["INTERNAL_DATA"] = invoice.INTERNAL_DATA.ToString();
                myExport["RECEIPT_SIGNATURE"] = invoice.RECEIPT_SIGNATURE.ToString();
                myExport["CIS_DATE"] = invoice.CIS_DATE.ToString();
                myExport["CIS_TIME"] = invoice.CIS_TIME.ToString();
                myExport["RECEIPT_TYPE_MRC"] = invoice.RECEIPT_TYPE_MRC.ToString();
                myExport["TIN"] = invoice.TIN.ToString();
                myExport["RECEIPT_NUMBER"] = invoice.RECEIPT_NUMBER.ToString();
                myExport["TAXRATE1"] = invoice.TAXRATE1.ToString();
                myExport["TAXRRATE2"] = invoice.TAXRRATE2.ToString();
                myExport["TAXRATE3"] = invoice.TAXRATE3.ToString();
                myExport["TAXRATE4"] = invoice.TAXRATE4.ToString();
                myExport["AMOUNT1"] = invoice.AMOUNT1.ToString();
                myExport["AMOUNT2"] = invoice.AMOUNT2.ToString();
                myExport["AMOUNT3"] = invoice.AMOUNT3.ToString();
                myExport["AMOUNT4"] = invoice.AMOUNT4.ToString();
                myExport["TAX1"] = invoice.TAX1.ToString();
                myExport["TAX2"] = invoice.TAX2.ToString();
                myExport["TAX3"] = invoice.TAX3.ToString();
                myExport["TAX4"] = invoice.TAX4.ToString();
                myExport["MRC"] = invoice.MRC.ToString();
                myExport["STATUS"] = invoice.STATUS.ToString();
                myExport["RESULTS"] = invoice.RESULTS.ToString();
                myExport["CREATED_AT"] = invoice.CREATED_AT.ToString();
                myExport["UPDATED_D_AT"] = invoice.UPDATED_D_AT.ToString();
            }

            /// Then you can do any of the following three output options:
            string myCsv = myExport.Export();

            byte[] myCsvData = myExport.ExportToBytes();

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.FileName = "Proccessed-invoices-" + DateTime.Now.ToString("dd-M-yyyy") + ".csv";
                if (DialogResult.OK != saveFileDialog.ShowDialog())
                    return;
                File.WriteAllBytes(saveFileDialog.FileName, myCsvData);
            }
        }
    }
}
