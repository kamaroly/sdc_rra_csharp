﻿using SDCCommunicator.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDCCommunicator
{
    public partial class SDCSettings : Form
    {
        SerialPort serialPort = new SerialPort();
        string pathName = System.Environment.CurrentDirectory;
        INIFile inif = new INIFile("\\config.ini");

        public SDCSettings()
        {
            InitializeComponent();
            this.inif = new INIFile(this.pathName + "\\config.ini");
        }

        private void btnShowPorts_Click(object sender, EventArgs e)
        {
            this.initializeAvailableComports();
        }

        /**
         * This shows all the devices that appear as com ports
         */
        public void initializeAvailableComports()
        {
            string[] availableComPorts = null; // Stores the available com ports on this computer
            int index = 1;
            string comPortName = null;

            // Make sure that we clean the rich text box

            // Attempt to get available ports on this computer
            ListPortsCbox.Items.Clear();

            availableComPorts = SerialPort.GetPortNames();
            do
            {
                index += 1;
                ListPortsCbox.Items.Add(availableComPorts[index]);
            }
            while (!((availableComPorts[index] == comPortName || (availableComPorts.GetUpperBound(0) == index))));

            // Display the first element and show it in the
            // com box 

           
            string portName = this.inif.Read("SDC", "PORT").ToUpper();
            ListPortsCbox.Text = portName;
        }

        private void SDCSettings_Load(object sender, EventArgs e)
        {
            this.loadModes();
            this.initializeAvailableComports();
            TinText.Text = this.inif.Read("SDC", "TIN");
            MRCtxt.Text = this.inif.Read("SDC", "MRC");
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            // Code
            this.setSdcSettings();
        } 
        private void loadModes()
        {
            ModesCbox.Items.Add("Test");
            ModesCbox.Items.Add("Live");
            string Mode = this.inif.Read("SDC", "MODE");
            ModesCbox.Text = Mode;
        }

        private void setSdcSettings()
        {
            this.inif.Write("SDC", "PORT", ListPortsCbox.Text);
            this.inif.Write("SDC", "TIN", TinText.Text);
            this.inif.Write("SDC", "MRC", MRCtxt.Text);
            this.inif.Write("SDC", "MODE", ModesCbox.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.setSdcSettings();
        }

    }
}
