﻿namespace SDCCommunicator
{
    partial class SDCSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListPortsCbox = new System.Windows.Forms.ComboBox();
            this.btnShowPorts = new System.Windows.Forms.Button();
            this.TinLebel = new System.Windows.Forms.Label();
            this.TinText = new System.Windows.Forms.TextBox();
            this.MRCtxt = new System.Windows.Forms.TextBox();
            this.MRCLbl = new System.Windows.Forms.Label();
            this.ModesCbox = new System.Windows.Forms.ComboBox();
            this.ModesLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ListPortsCbox
            // 
            this.ListPortsCbox.FormattingEnabled = true;
            this.ListPortsCbox.Location = new System.Drawing.Point(113, 14);
            this.ListPortsCbox.Name = "ListPortsCbox";
            this.ListPortsCbox.Size = new System.Drawing.Size(166, 21);
            this.ListPortsCbox.TabIndex = 3;
            // 
            // btnShowPorts
            // 
            this.btnShowPorts.Location = new System.Drawing.Point(12, 12);
            this.btnShowPorts.Name = "btnShowPorts";
            this.btnShowPorts.Size = new System.Drawing.Size(75, 23);
            this.btnShowPorts.TabIndex = 2;
            this.btnShowPorts.Text = "Show ports";
            this.btnShowPorts.UseVisualStyleBackColor = true;
            this.btnShowPorts.Click += new System.EventHandler(this.btnShowPorts_Click);
            // 
            // TinLebel
            // 
            this.TinLebel.AutoSize = true;
            this.TinLebel.Location = new System.Drawing.Point(12, 58);
            this.TinLebel.Name = "TinLebel";
            this.TinLebel.Size = new System.Drawing.Size(65, 13);
            this.TinLebel.TabIndex = 4;
            this.TinLebel.Text = "TIN Number";
            // 
            // TinText
            // 
            this.TinText.Location = new System.Drawing.Point(113, 58);
            this.TinText.Name = "TinText";
            this.TinText.Size = new System.Drawing.Size(166, 20);
            this.TinText.TabIndex = 5;
            this.TinText.Text = "100600570";
            // 
            // MRCtxt
            // 
            this.MRCtxt.Location = new System.Drawing.Point(113, 94);
            this.MRCtxt.Name = "MRCtxt";
            this.MRCtxt.Size = new System.Drawing.Size(166, 20);
            this.MRCtxt.TabIndex = 7;
            this.MRCtxt.Text = "01012345";
            // 
            // MRCLbl
            // 
            this.MRCLbl.AutoSize = true;
            this.MRCLbl.Location = new System.Drawing.Point(12, 97);
            this.MRCLbl.Name = "MRCLbl";
            this.MRCLbl.Size = new System.Drawing.Size(31, 13);
            this.MRCLbl.TabIndex = 6;
            this.MRCLbl.Text = "MRC";
            // 
            // ModesCbox
            // 
            this.ModesCbox.FormattingEnabled = true;
            this.ModesCbox.Location = new System.Drawing.Point(113, 132);
            this.ModesCbox.Name = "ModesCbox";
            this.ModesCbox.Size = new System.Drawing.Size(121, 21);
            this.ModesCbox.TabIndex = 8;
            this.ModesCbox.Visible = false;
            // 
            // ModesLbl
            // 
            this.ModesLbl.AutoSize = true;
            this.ModesLbl.Location = new System.Drawing.Point(12, 140);
            this.ModesLbl.Name = "ModesLbl";
            this.ModesLbl.Size = new System.Drawing.Size(83, 13);
            this.ModesLbl.TabIndex = 9;
            this.ModesLbl.Text = "Operation Mode";
            this.ModesLbl.Visible = false;
            // 
            // SDCSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 182);
            this.Controls.Add(this.ModesLbl);
            this.Controls.Add(this.ModesCbox);
            this.Controls.Add(this.MRCtxt);
            this.Controls.Add(this.MRCLbl);
            this.Controls.Add(this.TinText);
            this.Controls.Add(this.TinLebel);
            this.Controls.Add(this.ListPortsCbox);
            this.Controls.Add(this.btnShowPorts);
            this.Name = "SDCSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SDCSettings";
            this.Load += new System.EventHandler(this.SDCSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox ListPortsCbox;
        public System.Windows.Forms.Button btnShowPorts;
        public System.Windows.Forms.Label TinLebel;
        public System.Windows.Forms.TextBox TinText;
        public System.Windows.Forms.TextBox MRCtxt;
        public System.Windows.Forms.Label MRCLbl;
        public System.Windows.Forms.ComboBox ModesCbox;
        public System.Windows.Forms.Label ModesLbl;
    }
}