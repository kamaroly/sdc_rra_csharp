﻿namespace SDCCommunicator
{
    partial class DatabaseSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DbHostLbl = new System.Windows.Forms.Label();
            this.DbHostTxt = new System.Windows.Forms.TextBox();
            this.DBNametxt = new System.Windows.Forms.TextBox();
            this.DBNameLbl = new System.Windows.Forms.Label();
            this.DBUsertxt = new System.Windows.Forms.TextBox();
            this.DbUserLbl = new System.Windows.Forms.Label();
            this.DBPasstxt = new System.Windows.Forms.TextBox();
            this.DbPassLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DbHostLbl
            // 
            this.DbHostLbl.AutoSize = true;
            this.DbHostLbl.Location = new System.Drawing.Point(12, 31);
            this.DbHostLbl.Name = "DbHostLbl";
            this.DbHostLbl.Size = new System.Drawing.Size(76, 13);
            this.DbHostLbl.TabIndex = 0;
            this.DbHostLbl.Text = "Database host";
            // 
            // DbHostTxt
            // 
            this.DbHostTxt.Location = new System.Drawing.Point(94, 28);
            this.DbHostTxt.Name = "DbHostTxt";
            this.DbHostTxt.Size = new System.Drawing.Size(149, 20);
            this.DbHostTxt.TabIndex = 1;
            // 
            // DBNametxt
            // 
            this.DBNametxt.Location = new System.Drawing.Point(94, 64);
            this.DBNametxt.Name = "DBNametxt";
            this.DBNametxt.Size = new System.Drawing.Size(149, 20);
            this.DBNametxt.TabIndex = 3;
            // 
            // DBNameLbl
            // 
            this.DBNameLbl.AutoSize = true;
            this.DBNameLbl.Location = new System.Drawing.Point(12, 67);
            this.DBNameLbl.Name = "DBNameLbl";
            this.DBNameLbl.Size = new System.Drawing.Size(82, 13);
            this.DBNameLbl.TabIndex = 2;
            this.DBNameLbl.Text = "Database name";
            // 
            // DBUsertxt
            // 
            this.DBUsertxt.Location = new System.Drawing.Point(94, 104);
            this.DBUsertxt.Name = "DBUsertxt";
            this.DBUsertxt.Size = new System.Drawing.Size(149, 20);
            this.DBUsertxt.TabIndex = 5;
            // 
            // DbUserLbl
            // 
            this.DbUserLbl.AutoSize = true;
            this.DbUserLbl.Location = new System.Drawing.Point(12, 107);
            this.DbUserLbl.Name = "DbUserLbl";
            this.DbUserLbl.Size = new System.Drawing.Size(76, 13);
            this.DbUserLbl.TabIndex = 4;
            this.DbUserLbl.Text = "Database user";
            // 
            // DBPasstxt
            // 
            this.DBPasstxt.Location = new System.Drawing.Point(94, 141);
            this.DBPasstxt.Name = "DBPasstxt";
            this.DBPasstxt.PasswordChar = '*';
            this.DBPasstxt.Size = new System.Drawing.Size(149, 20);
            this.DBPasstxt.TabIndex = 7;
            // 
            // DbPassLbl
            // 
            this.DbPassLbl.AutoSize = true;
            this.DbPassLbl.Location = new System.Drawing.Point(12, 144);
            this.DbPassLbl.Name = "DbPassLbl";
            this.DbPassLbl.Size = new System.Drawing.Size(78, 13);
            this.DbPassLbl.TabIndex = 6;
            this.DbPassLbl.Text = "Database pass";
            // 
            // DatabaseSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 181);
            this.Controls.Add(this.DBPasstxt);
            this.Controls.Add(this.DbPassLbl);
            this.Controls.Add(this.DBUsertxt);
            this.Controls.Add(this.DbUserLbl);
            this.Controls.Add(this.DBNametxt);
            this.Controls.Add(this.DBNameLbl);
            this.Controls.Add(this.DbHostTxt);
            this.Controls.Add(this.DbHostLbl);
            this.Name = "DatabaseSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DatabaseSettings";
            this.Load += new System.EventHandler(this.DatabaseSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DbHostLbl;
        private System.Windows.Forms.TextBox DbHostTxt;
        private System.Windows.Forms.TextBox DBNametxt;
        private System.Windows.Forms.Label DBNameLbl;
        private System.Windows.Forms.TextBox DBUsertxt;
        private System.Windows.Forms.Label DbUserLbl;
        private System.Windows.Forms.TextBox DBPasstxt;
        private System.Windows.Forms.Label DbPassLbl;
    }
}