
Account Number:7187                                                     Company: TIGO RWANDA LTD
ABDOULAHI                                                 Address:Kigali, Rwanda
KIGALI                                                              TIN: 101775989
Type of Account :H                                         Tel.(+250)722123000
Payment Due Date : 5/25/2016

                                    INVOICE NUMBER 04302016C007187
ITEM  NAME                                                                        AMOUNT
----------------------------------------------------------------------------------------------
Balance Brought Forward                                                           RWF 20000 
Adjustments                                                                       RWF 0
Payments Received                                                                 RWF 0
                                                                                 --------------
TOTAL OUTSTANDING FROM PREVIOUS PERIODS                                           RWF 20000               
MONTHLY CHARGES(All Taxes Included)
Total Call Entitlement Charges                                                    RWF 
CUG and Feature Charges                                                           RWF RWF0.00
Printed Bill                                                                      RWF 0
Late Charges                                                                      RWF 
Other Charges                                                                     RWF 
Discounts                                                                         RWF 0
                                                                                 --------------
TOTAL CHARGES FOR THE MONTH                                                       RWF 0
TOTAL AMOUNT DUE                                                                  RWF 20000
ACTIVITY DETAILS
Total Additional Recharges                                                        RWF RWF0.00
Voice Local                                                                       RWF 0
SMS                                                                               RWF 
DATA                                                                              RWF 
VAS                                                                               RWF 0
Voice International                                                               RWF 
Roaming                                                                           RWF 
----------------------------------------------------------------------------------------------
                                              TOTAL                               20000
                                              TOTAL B-18.00%                      0
                                              TOTAL TAX B                         0
                                              TOTAL TAX                           0
                                              ------------------------------------------------
                                              CASH                                      0
                                              ITEMS NUMBER                              11
                                              ------------------------------------------------
                                                               SDC INFORMATION                  
                                              Date: 20/06/2016                Time: 17:01:17
                                              SDC ID:                             SDC002001531
                                              RECEIPT NUMBER:             208/281 NS
                                              			           Internal Data:  
                                                      U4M6-CCJL-KCOP-QKEX-JUN3-DZPI-HE       
                                                             Receipt Signature:                
                                                            FPFR-2CIH-5QQL-GTZO                
                                              -----------------------------------------------
                                              RECEIPT NUMBER:                      04302016C007187    
                                              DATE: 31/05/2016                TIME:09:21:27
                                              MRC:                                 01012345
                                              -----------------------------------------------