﻿namespace SerialComms
{
    partial class MainWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindows));
            this.lblCTSStatus = new System.Windows.Forms.Label();
            this.lblDSRStatus = new System.Windows.Forms.Label();
            this.lblRIStatus = new System.Windows.Forms.Label();
            this.lblBreakStatus = new System.Windows.Forms.Label();
            this.richTextCommand = new System.Windows.Forms.RichTextBox();
            this.richTextResponse = new System.Windows.Forms.RichTextBox();
            this.SDC_ID_Status = new System.Windows.Forms.Button();
            this.SDCIDcmd = new System.Windows.Forms.Button();
            this.ReceiptDataToSDC = new System.Windows.Forms.Button();
            this.requestSignature = new System.Windows.Forms.Button();
            this.getCounterButton = new System.Windows.Forms.Button();
            this.SendElectronicJournalcmd = new System.Windows.Forms.Button();
            this.inputLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sDCSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.batch_proccess = new System.Windows.Forms.Button();
            this.progressBarBulk = new System.Windows.Forms.ProgressBar();
            this.UnprocessedLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRefund = new System.Windows.Forms.Button();
            this.Process_single_bill = new System.Windows.Forms.Button();
            this.TAXA = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.xZReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pLUReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.mMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCTSStatus
            // 
            this.lblCTSStatus.Location = new System.Drawing.Point(0, 0);
            this.lblCTSStatus.Name = "lblCTSStatus";
            this.lblCTSStatus.Size = new System.Drawing.Size(100, 23);
            this.lblCTSStatus.TabIndex = 3;
            // 
            // lblDSRStatus
            // 
            this.lblDSRStatus.Location = new System.Drawing.Point(0, 0);
            this.lblDSRStatus.Name = "lblDSRStatus";
            this.lblDSRStatus.Size = new System.Drawing.Size(100, 23);
            this.lblDSRStatus.TabIndex = 2;
            // 
            // lblRIStatus
            // 
            this.lblRIStatus.Location = new System.Drawing.Point(0, 0);
            this.lblRIStatus.Name = "lblRIStatus";
            this.lblRIStatus.Size = new System.Drawing.Size(100, 23);
            this.lblRIStatus.TabIndex = 1;
            // 
            // lblBreakStatus
            // 
            this.lblBreakStatus.Location = new System.Drawing.Point(0, 0);
            this.lblBreakStatus.Name = "lblBreakStatus";
            this.lblBreakStatus.Size = new System.Drawing.Size(100, 23);
            this.lblBreakStatus.TabIndex = 0;
            // 
            // richTextCommand
            // 
            this.richTextCommand.ForeColor = System.Drawing.Color.DarkRed;
            this.richTextCommand.Location = new System.Drawing.Point(217, 52);
            this.richTextCommand.Name = "richTextCommand";
            this.richTextCommand.Size = new System.Drawing.Size(477, 49);
            this.richTextCommand.TabIndex = 9;
            this.richTextCommand.Text = "";
            // 
            // richTextResponse
            // 
            this.richTextResponse.ForeColor = System.Drawing.Color.Navy;
            this.richTextResponse.Location = new System.Drawing.Point(217, 133);
            this.richTextResponse.Name = "richTextResponse";
            this.richTextResponse.Size = new System.Drawing.Size(477, 163);
            this.richTextResponse.TabIndex = 16;
            this.richTextResponse.Text = "";
            // 
            // SDC_ID_Status
            // 
            this.SDC_ID_Status.Location = new System.Drawing.Point(12, 62);
            this.SDC_ID_Status.Name = "SDC_ID_Status";
            this.SDC_ID_Status.Size = new System.Drawing.Size(166, 23);
            this.SDC_ID_Status.TabIndex = 0;
            this.SDC_ID_Status.Text = "Get SDC status";
            this.SDC_ID_Status.Click += new System.EventHandler(this.SDC_ID_Status_Click);
            // 
            // SDCIDcmd
            // 
            this.SDCIDcmd.Location = new System.Drawing.Point(12, 33);
            this.SDCIDcmd.Name = "SDCIDcmd";
            this.SDCIDcmd.Size = new System.Drawing.Size(166, 23);
            this.SDCIDcmd.TabIndex = 12;
            this.SDCIDcmd.Text = "Get SDC ID";
            this.SDCIDcmd.UseVisualStyleBackColor = true;
            this.SDCIDcmd.Click += new System.EventHandler(this.SDCIDcmd_Click);
            // 
            // ReceiptDataToSDC
            // 
            this.ReceiptDataToSDC.Location = new System.Drawing.Point(12, 91);
            this.ReceiptDataToSDC.Name = "ReceiptDataToSDC";
            this.ReceiptDataToSDC.Size = new System.Drawing.Size(166, 33);
            this.ReceiptDataToSDC.TabIndex = 13;
            this.ReceiptDataToSDC.Text = "Send Receipt Data to SDC";
            this.ReceiptDataToSDC.Click += new System.EventHandler(this.ReceiptDataToSDC_Click);
            // 
            // requestSignature
            // 
            this.requestSignature.Location = new System.Drawing.Point(12, 130);
            this.requestSignature.Name = "requestSignature";
            this.requestSignature.Size = new System.Drawing.Size(166, 33);
            this.requestSignature.TabIndex = 17;
            this.requestSignature.Text = "Request SIGNATURE";
            this.requestSignature.Click += new System.EventHandler(this.requestSignature_Click);
            // 
            // getCounterButton
            // 
            this.getCounterButton.Location = new System.Drawing.Point(12, 210);
            this.getCounterButton.Name = "getCounterButton";
            this.getCounterButton.Size = new System.Drawing.Size(166, 41);
            this.getCounterButton.TabIndex = 0;
            this.getCounterButton.Text = "Get counter";
            this.getCounterButton.Click += new System.EventHandler(this.getCounterButton_Click);
            // 
            // SendElectronicJournalcmd
            // 
            this.SendElectronicJournalcmd.Location = new System.Drawing.Point(12, 169);
            this.SendElectronicJournalcmd.Name = "SendElectronicJournalcmd";
            this.SendElectronicJournalcmd.Size = new System.Drawing.Size(166, 33);
            this.SendElectronicJournalcmd.TabIndex = 18;
            this.SendElectronicJournalcmd.Text = "Send Electronic journal";
            this.SendElectronicJournalcmd.Click += new System.EventHandler(this.SendElectronicJournalcmd_Click);
            // 
            // inputLabel
            // 
            this.inputLabel.AutoSize = true;
            this.inputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.inputLabel.ForeColor = System.Drawing.Color.DarkOrange;
            this.inputLabel.Location = new System.Drawing.Point(213, 29);
            this.inputLabel.Name = "inputLabel";
            this.inputLabel.Size = new System.Drawing.Size(139, 20);
            this.inputLabel.TabIndex = 19;
            this.inputLabel.Text = "Command input";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(213, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 20);
            this.label3.TabIndex = 20;
            this.label3.Text = "Response";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(117, 26);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // mMenu
            // 
            this.mMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.configurationToolStripMenuItem});
            this.mMenu.Location = new System.Drawing.Point(0, 0);
            this.mMenu.Name = "mMenu";
            this.mMenu.Size = new System.Drawing.Size(848, 24);
            this.mMenu.TabIndex = 24;
            this.mMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zReportToolStripMenuItem,
            this.yReportToolStripMenuItem,
            this.xZReportToolStripMenuItem,
            this.pLUReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // zReportToolStripMenuItem
            // 
            this.zReportToolStripMenuItem.Name = "zReportToolStripMenuItem";
            this.zReportToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.zReportToolStripMenuItem.Text = "Unprocessed invoices";
            this.zReportToolStripMenuItem.Click += new System.EventHandler(this.zReportToolStripMenuItem_Click);
            // 
            // yReportToolStripMenuItem
            // 
            this.yReportToolStripMenuItem.Name = "yReportToolStripMenuItem";
            this.yReportToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.yReportToolStripMenuItem.Text = "Processed invoices";
            this.yReportToolStripMenuItem.Click += new System.EventHandler(this.yReportToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sDCSettingsToolStripMenuItem,
            this.databaseSettingsToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // sDCSettingsToolStripMenuItem
            // 
            this.sDCSettingsToolStripMenuItem.Name = "sDCSettingsToolStripMenuItem";
            this.sDCSettingsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.sDCSettingsToolStripMenuItem.Text = " SDC Settings";
            this.sDCSettingsToolStripMenuItem.Click += new System.EventHandler(this.sDCSettingsToolStripMenuItem_Click);
            // 
            // databaseSettingsToolStripMenuItem
            // 
            this.databaseSettingsToolStripMenuItem.Name = "databaseSettingsToolStripMenuItem";
            this.databaseSettingsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.databaseSettingsToolStripMenuItem.Text = "Database Settings";
            this.databaseSettingsToolStripMenuItem.Click += new System.EventHandler(this.databaseSettingsToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 41);
            this.button1.TabIndex = 27;
            this.button1.Text = "Generate Invoice";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // batch_proccess
            // 
            this.batch_proccess.BackColor = System.Drawing.Color.Lime;
            this.batch_proccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batch_proccess.ForeColor = System.Drawing.Color.Black;
            this.batch_proccess.Location = new System.Drawing.Point(13, 305);
            this.batch_proccess.Name = "batch_proccess";
            this.batch_proccess.Size = new System.Drawing.Size(165, 39);
            this.batch_proccess.TabIndex = 28;
            this.batch_proccess.Text = "Batch processing";
            this.batch_proccess.UseVisualStyleBackColor = false;
            this.batch_proccess.Click += new System.EventHandler(this.batch_proccess_Click);
            // 
            // progressBarBulk
            // 
            this.progressBarBulk.Location = new System.Drawing.Point(217, 305);
            this.progressBarBulk.Name = "progressBarBulk";
            this.progressBarBulk.Size = new System.Drawing.Size(379, 38);
            this.progressBarBulk.TabIndex = 29;
            // 
            // UnprocessedLabel
            // 
            this.UnprocessedLabel.AutoSize = true;
            this.UnprocessedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnprocessedLabel.Location = new System.Drawing.Point(627, 299);
            this.UnprocessedLabel.Name = "UnprocessedLabel";
            this.UnprocessedLabel.Size = new System.Drawing.Size(32, 36);
            this.UnprocessedLabel.TabIndex = 30;
            this.UnprocessedLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(595, 330);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Remaining invoices";
            // 
            // btnRefund
            // 
            this.btnRefund.BackColor = System.Drawing.Color.DarkOrange;
            this.btnRefund.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefund.ForeColor = System.Drawing.Color.Cornsilk;
            this.btnRefund.Location = new System.Drawing.Point(700, 122);
            this.btnRefund.Name = "btnRefund";
            this.btnRefund.Size = new System.Drawing.Size(135, 49);
            this.btnRefund.TabIndex = 32;
            this.btnRefund.Text = "Refund bill";
            this.btnRefund.UseVisualStyleBackColor = false;
            this.btnRefund.Click += new System.EventHandler(this.btnRefund_Click);
            // 
            // Process_single_bill
            // 
            this.Process_single_bill.BackColor = System.Drawing.Color.DarkGreen;
            this.Process_single_bill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Process_single_bill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Process_single_bill.ForeColor = System.Drawing.Color.White;
            this.Process_single_bill.Location = new System.Drawing.Point(700, 49);
            this.Process_single_bill.Name = "Process_single_bill";
            this.Process_single_bill.Size = new System.Drawing.Size(135, 49);
            this.Process_single_bill.TabIndex = 33;
            this.Process_single_bill.Text = "Process a bill";
            this.Process_single_bill.UseVisualStyleBackColor = false;
            this.Process_single_bill.Click += new System.EventHandler(this.Process_single_bill_Click);
            // 
            // TAXA
            // 
            this.TAXA.AutoSize = true;
            this.TAXA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TAXA.Location = new System.Drawing.Point(700, 201);
            this.TAXA.Name = "TAXA";
            this.TAXA.Size = new System.Drawing.Size(47, 13);
            this.TAXA.TabIndex = 34;
            this.TAXA.Text = "TAX A:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(701, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "TAX B:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(700, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "TAX C:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(701, 271);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "TAX D:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkOrange;
            this.label2.Location = new System.Drawing.Point(696, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 16);
            this.label2.TabIndex = 38;
            this.label2.Text = "TAX RATE LABELS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(750, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "EX";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(750, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "18%";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(749, 247);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "0%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(751, 271);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 42;
            this.label11.Text = "0%";
            // 
            // xZReportToolStripMenuItem
            // 
            this.xZReportToolStripMenuItem.Name = "xZReportToolStripMenuItem";
            this.xZReportToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.xZReportToolStripMenuItem.Text = "X & Z Report";
            this.xZReportToolStripMenuItem.Click += new System.EventHandler(this.xZReportToolStripMenuItem_Click);
            // 
            // pLUReportToolStripMenuItem
            // 
            this.pLUReportToolStripMenuItem.Name = "pLUReportToolStripMenuItem";
            this.pLUReportToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.pLUReportToolStripMenuItem.Text = "PLU Report";
            this.pLUReportToolStripMenuItem.Click += new System.EventHandler(this.pLUReportToolStripMenuItem_Click);
            // 
            // MainWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(848, 356);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TAXA);
            this.Controls.Add(this.Process_single_bill);
            this.Controls.Add(this.btnRefund);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UnprocessedLabel);
            this.Controls.Add(this.progressBarBulk);
            this.Controls.Add(this.batch_proccess);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.mMenu);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputLabel);
            this.Controls.Add(this.SendElectronicJournalcmd);
            this.Controls.Add(this.getCounterButton);
            this.Controls.Add(this.requestSignature);
            this.Controls.Add(this.ReceiptDataToSDC);
            this.Controls.Add(this.SDCIDcmd);
            this.Controls.Add(this.SDC_ID_Status);
            this.Controls.Add(this.richTextResponse);
            this.Controls.Add(this.richTextCommand);
            this.Controls.Add(this.lblBreakStatus);
            this.Controls.Add(this.lblRIStatus);
            this.Controls.Add(this.lblDSRStatus);
            this.Controls.Add(this.lblCTSStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindows";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CIS to SDC BRIDGE (VERSION 1.0)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.mMenu.ResumeLayout(false);
            this.mMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCTSStatus;
        private System.Windows.Forms.Label lblDSRStatus;
        private System.Windows.Forms.Label lblRIStatus;
        private System.Windows.Forms.Label lblBreakStatus;
        private System.Windows.Forms.RichTextBox richTextCommand;
        private System.Windows.Forms.RichTextBox richTextResponse;
        private System.Windows.Forms.Button SDC_ID_Status;
        private System.Windows.Forms.Button SDCIDcmd;
        private System.Windows.Forms.Button ReceiptDataToSDC;
        private System.Windows.Forms.Button requestSignature;
        private System.Windows.Forms.Button getCounterButton;
        private System.Windows.Forms.Button SendElectronicJournalcmd;
        private System.Windows.Forms.Label inputLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.MenuStrip mMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sDCSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yReportToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button batch_proccess;
        private System.Windows.Forms.ProgressBar progressBarBulk;
        private System.Windows.Forms.Label UnprocessedLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRefund;
        private System.Windows.Forms.Button Process_single_bill;
        private System.Windows.Forms.Label TAXA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripMenuItem xZReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pLUReportToolStripMenuItem;
    }
}

