﻿using SDCCommunicator.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDCCommunicator
{
    public partial class DatabaseSettings : Form
    {
        string pathName = System.Environment.CurrentDirectory;
        INIFile inif = new INIFile("\\config.ini");

        public DatabaseSettings()
        {
            InitializeComponent();
            this.inif = new INIFile(this.pathName + "\\config.ini");
            this.loadDBSetting();
        }

     

        private void DatabaseSettings_Load(object sender, EventArgs e)
        {
            this.loadDBSetting();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            this.setDBSettings();
        }

        private void loadDBSetting()
        {
            DbHostTxt.Text = this.inif.Read("DATABASE", "DBHOST");
            DBNametxt.Text = this.inif.Read("DATABASE", "DBNAME");
            DBUsertxt.Text = this.inif.Read("DATABASE", "DBUSER");
            DBPasstxt.Text = this.inif.Read("DATABASE", "DBPASS");
        }

        private void setDBSettings()
        {
            this.inif.Write("DATABASE", "DBHOST",DbHostTxt.Text );
            this.inif.Write("DATABASE", "DBNAME", DBNametxt.Text);
            this.inif.Write("DATABASE", "DBUSER",DBUsertxt.Text);
            this.inif.Write("DATABASE", "DBPASS",DBPasstxt.Text);     
        }
    }
}
