﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using SerialComms.Logs;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Diagnostics;
using SDCCommunicator.Helpers;
using SDCCommunicator.Repository;
using SDCCommunicator;
using SDCCommunicator.Factories;
using SDCCommunicator.Models;
using Microsoft.VisualBasic;

namespace SerialComms
{
    public partial class MainWindows : Form
    {
        SerialPort serialPort = new SerialPort();
        string pathName = System.Environment.CurrentDirectory;
        INIFile inif = new INIFile("\\Config.ini");
        private SDCSettings sdcSettings = new SDCSettings();
      
        public MainWindows()
        {
            this.KeyPreview = true;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.unProccessed();
        }

        /**
         * @Author  : Kamaro Lambert 
         * @method  : to get SDC ID
         * @COMMAND : E5
        */
        public Dictionary<string, string> getSdcId()
        {
            return SDCFactory.getSdcId();
        }

        /**
         * @method  : to get SDC STATUS
        */
        public  Dictionary<string, string> getSdcStatus()
        {
            return SDCFactory.getSdcStatus();
        }

        /**
         * @method  : To request recept counters from SDC
        */
        public Dictionary<string, string> getCounters(string data)
        {
            return SDCFactory.getCounters(data);
        }

        /**
         * @method  : this method is used to send electronic journal to RRA
        */ 
        public Dictionary<string, string> getSignature(string data)
        {
            return SDCFactory.getSignature(data);
        }

        /**
         * @method  : this method is used to send electronic journal to RRA
         */
        public Dictionary<string, string> sendReceiptData(string data)
        {
            return SDCFactory.sendReceiptData(data);
        }

        /**
         * @method : this method is used to send electronic journal to RRA
         */ 
        //public Dictionary<string, string> sendElectronicJournal()
        //{
        //    return SDCFactory.sendElectronicJournal();
        //}
       
      
        /**
         * Get SDC request method
         * @Author Kamaro Lambert
         * @param string data //  RtypeTTypeMRC,TIN,Date TIME, Rnumber,TaxRate1,TaxrRate2,TaxRate3,TaxRate4,Amount1,Amount2,Amount3,Amount4,Tax1,Tax2,Tax3,Tax4
         *                    // Example : "nstes01012345,100600570,17/07/2013 09:29:37,1,0.00,18.00,0.00,0.00,11.00,12.00,0.00,0.00,0.00,1.83,0.00,0.00"
         * @param string command // Command to sdc example c6
         */
        private string getSdcRequest(string data, string command,string sequence)
        {
            string commandLength;   // Length of the command
            string commandBcc;      // CheckSum of the command
            string request;

            // Make sure ALL are in  caps
            data = data.ToUpper();        
            data = this.getHexData(data);
            command = command.ToUpper();
            request = sequence + " " + command.ToUpper() + " 05";

            // if the data is not empty then add it to the command
            if (string.IsNullOrWhiteSpace(data) == false)
            {
                request = sequence + " " + command.ToUpper() + " " + data + " 05";
            }
            

            // Get the length of the byte hex to be sent
            commandLength = this.getLength(data);

            // Add length to the request
            request = commandLength + " " + request;
            
            // Get checksum(BCC) of this command
            commandBcc = this.getBcc(request);  

            // For example to look for serial number you have to pass "01 24 20 E5 05 30 31 32 3E 03" OR 
            // SDC Status "01 24 20 E7 05 30 31 33 30 03"
            return request = "01" + " " + request + " " + commandBcc + " 03";
        }

        /**ret
         * Method to generate command to be sent to SDC
         */
        private string getHexData(string dataString)
        {
          // First you'll need to get it into a byte[], so do this:
          byte[] bytes = Encoding.Default.GetBytes(dataString);

          //and then you can get the string:
          string hexString = BitConverter.ToString(bytes);
           
          // now, that's going to return a string with dashes (-) in it so you can then simply use this:
          return hexString.Replace("-", " ").ToUpper();
        }

        /**
         * Number of bytes from <01> (excluded) to <05> (included) plus a fixed offset
         * 20h Length: 1 byte;
         * 
         * @Author Lambert Kamaro
         */
        private string getLength(string data)
        {     
            // Find the length by counting the data and adding length itsself, 
            // sequence,command,Post amble 05 (TOTAL=4)
            //, and 20h which is 32 in decimal which is 36 in total
            int length = 36;
 
            if (!string.IsNullOrWhiteSpace(data))
            {
                // Make sure that data is in capital letter 
                data = data.ToUpper();
                byte[] bytes = data.Split(' ').Select(s => Convert.ToByte(s, 16)).ToArray();
                length += bytes.Length;
            }

            byte[] lengthArray = BitConverter.GetBytes(length);
            lengthArray = this.Decode(lengthArray);
            string hex = BitConverter.ToString(lengthArray);

            return hex.ToUpper();
        }

           /**
           * @Author Kamaro Lambert
           * Method to get the BCC or the HEX to send to SDC
           * ===============================================
           * Check sum (0000h-FFFFh)
           * Length: 4 bytes; value: 30h - 3Fh
           * The check sum is formed by the bytes following <01> (without it) to <05>
           * included, by summing (adding) the values of the bytes. Each digit is sent as
           * an ASCII code.
           * =============================================== 
           * @param string $string sum of hex bytes between 01 excluded and 05 included
           * @return string
           */
        private string getBcc(string dataWithLengthAndCommand)
        {
            // First make sure the string is in capital
            //dataWithLengthAndCommand = dataWithLengthAndCommand.ToUpper();
            string[] dataArray = dataWithLengthAndCommand.Split(' ');
            int checkSum = 0; // This will hold the sum of values of the bytes
            foreach (string hexBit in dataArray)
            {
                int asciiValue = Convert.ToInt32(hexBit, 16);
                checkSum += asciiValue;
            }

            string checkHex = string.Format("{0:X2}", checkSum);
            // Use ToCharArray to convert string to array.
            char[] array = checkHex.ToCharArray();
            int hexArraySize = (checkHex.Length - 4) * -1;
            string[] checkSumString = new string[hexArraySize + checkHex.Length];

            // Fill empty bits with 30 as 30 is the minimum value
            int index = hexArraySize;
            hexArraySize = hexArraySize - 1;
            while (hexArraySize >= 0)
            {
                checkSumString[hexArraySize] = "30";
                hexArraySize--;
            }

            // Fill the rest of the array.
            for (int i = 0; i < array.Length; i++)
            {
                // Get character from array.
                checkSumString[index++] = "3" + array[i].ToString();
            }
            // Conver the checkSum to the it's corresponding hex value
            string checkSumBcc = string.Join(" ", checkSumString);

            return checkSumBcc.ToUpper();   
        }

        /**
         * Remove 00 data from this array
         */
        public byte[] Decode(byte[] packet)
        {
            var i = packet.Length - 1;
            while (packet[i] == 0)
            {
                --i;
            }
            var temp = new byte[i + 1];
            Array.Copy(packet, temp, i + 1);
            return temp;
        }

        private void SDCIDcmd_Click(object sender, EventArgs e)
        {
            SdcRepository sdcRepository = new SdcRepository();
            Dictionary<string, string> responseDb = sdcRepository.Read();

            richTextResponse.Clear();
            Dictionary<string,string> response  = this.getSdcId();
            foreach (KeyValuePair<string, string> element in response)
            {
                richTextResponse.Text += element.Key + " : " + element.Value + "\r\n";
            }
        }

        private void SDC_ID_Status_Click(object sender, EventArgs e)
        {
            richTextResponse.Clear();
            Dictionary<string, string> response = this.getSdcStatus();
            foreach (KeyValuePair<string, string> element in response)
            {
                richTextResponse.Text += element.Key + " : " + element.Value + "\r\n";
            }
        }

        private void ReceiptDataToSDC_Click(object sender, EventArgs e)
        {
            string data = richTextCommand.Text;
            if (string.IsNullOrWhiteSpace(data))
            {
                richTextResponse.Text="Please provide receipt information, example :nstes01012345,100600570,17/07/2013 09:29:37,1,0.00,18.00,0.00,0.00,11.00,12.00,0.00,0.00,0.00,1.83,0.00,0.00";
            }
            else
            {
                richTextResponse.Clear();
                Dictionary<string, string> response = this.sendReceiptData(data);
                foreach (KeyValuePair<string, string> element in response)
                {
                    richTextResponse.Text += element.Key + " : " + element.Value + "\r\n";
                }
            }
        }

        private void getCounterButton_Click(object sender, EventArgs e)
        {
                richTextResponse.Clear();
                string data = richTextCommand.Text;
                Dictionary<string, string> response = this.getCounters(data);
                foreach (KeyValuePair<string, string> element in response)
                {
                    richTextResponse.Text += element.Key + " : " + element.Value + "\r\n";
                }

        }

        private void requestSignature_Click(object sender, EventArgs e)
        {
            string data = richTextCommand.Text;
            if (string.IsNullOrWhiteSpace(data))
            {
                richTextResponse.Text="Please provide receipt information, example : 1";
            }
            else
            {
                richTextResponse.Clear();
                Dictionary<string, string> response = this.getSignature(data);
                foreach (KeyValuePair<string, string> element in response)
                {
                    richTextResponse.Text += element.Key +" : " + element.Value +"\r\n";
                }
            }
        }

        private void SendElectronicJournalcmd_Click(object sender, EventArgs e)
        {
            //richTextResponse.Clear();
            //Dictionary<string, string> response = this.sendElectronicJournal();
            //foreach (KeyValuePair<string, string> element in response)
            //{
            //    richTextResponse.Text += element.Key + " : " + element.Value + "\r\n";
            //}
        }

        private void generatePDFbtn_Click(object sender, EventArgs e)
        {
            try
            {
                string line = null;
                System.IO.TextReader readFile = new System.IO.StreamReader("e:\\invoiceTemplate.txt");
                int yPoint = 0;

                PdfDocument pdf = new PdfDocument();
                pdf.Info.Title = "TXT to PDF";
                PdfPage pdfPage = pdf.AddPage();
                pdfPage.Size = PdfSharp.PageSize.A4;
                XGraphics graph = XGraphics.FromPdfPage(pdfPage);
                XFont font = new XFont("Verdana", 9, XFontStyle.Regular);

                while (true)
                {
                    line = readFile.ReadLine();
                    if (line == null)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }
                    else
                    {
                        graph.DrawString(line, font, XBrushes.Black, new XRect(40, yPoint, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.TopLeft);
                        yPoint = yPoint + 14;
                    }
                }

                DateTime datetime = new DateTime();
                string pdfFilename = datetime.ToString("Y-m-d-h-i-s");
                pdf.Save(pdfFilename);
                readFile.Close();
                readFile = null;
                Process.Start(pdfFilename);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        /**
         * Close main window
         */ 
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBoxListPorts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sDCSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Form sdcSettings = new SDCCommunicator.SDCSettings();
           sdcSettings.Show();
        }

        private void databaseSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form databaseSettings = new SDCCommunicator.DatabaseSettings();
            databaseSettings.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintInvoice print = new PrintInvoice();
            print.pdf();
        }

        /**
         * Process invoices
         */
        private void batch_proccess_Click(object sender, EventArgs e)
        {
            // FETCH INFORMATION 
            List<CISAvabillModel> invoices = CISAvabillModel.pickInvoices();

            // No need to continue if we have nothing to process
            if (invoices.Count() == 0)
            {
                MessageBox.Show("There is no invoice to proceeed");

                return;
            }

            string message = string.Empty;
            // SET INDICATORS
            this.progressBarBulk.Maximum = invoices.Count();
            
            foreach (CISAvabillModel invoice in invoices)
            {
                // if invoices has 0 francs don't proceed.
                if (Convert.ToInt32(invoice.TOTAL) < 1)
                {
                    MessageBox.Show("We detected invoice :" + invoice.INVOICE_NUMBER + " with 0 amount, we are skipping it, Click OK to proceed");
                    continue;
                }

                message = SDCFactory.bill(invoice);
                if (!message.ToLower().Equals("success"))
                {
                    MessageBox.Show(message);
                    return;
                }

                this.UnprocessedLabel.Text = (Convert.ToInt64(this.UnprocessedLabel.Text) - 1).ToString();
                this.progressBarBulk.Increment(1);
            }


            MessageBox.Show("TASK HAS BEEN COMPLETED");
        }

    

        private void unProccessed()
        {
            this.UnprocessedLabel.Text = CISAvabillModel.pickInvoices().Count().ToString();
        }

        // Download proccessed invoices
        private void zReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports.getUnproccessed();
        }

        private void yReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports.getProccessedInvoices();
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Please provide invoice number to process", "Refunding a bill", "", -1, -1);

            if (string.IsNullOrWhiteSpace(input))
            {
                MessageBox.Show("Please enter a valid receipt number");
                return;
            }
            // FETCH INFORMATION 
            List<CISAvabillModel> invoices = CISAvabillModel.pickInvoices(input.Trim());

            // No need to continue if we have nothing to process
            if (invoices.Count() == 0)
            {
                MessageBox.Show("There is no invoice to proceeed");

                return;
            }

            string message = string.Empty;

            foreach (CISAvabillModel invoice in invoices)
            {
                // Prepare data before processing it.
                invoice.RECEIPT_TYPE_MRC = "NR";
                invoice.ID = CISAvabillModel.getNextId();
              
                message = SDCFactory.bill(invoice);

                if (message.ToLower().Equals("success"))
                {
                    MessageBox.Show("REFUND FOR INVOICE WITH NUMBER:" + input + " HAS BEEN PROCESSED SUCCESSFULLY");
                    return;
                }

                // WE HAVE ERROR IN THE REQUEST SHOW THE ERROR
                MessageBox.Show(message + "|INVOICE NUMBER:" + invoice.INVOICE_NUMBER);
                return;
            }


            MessageBox.Show("TASK HAS BEEN COMPLETED");
        }

        private void Process_single_bill_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("Please provide invoice number to process", "PROCESSING A BILL", "", -1, -1);

            if (string.IsNullOrWhiteSpace(input))
            {
                MessageBox.Show("Please enter a valid receipt number");
                return;
            }
            // FETCH INFORMATION 
            List<CISAvabillModel> invoices = CISAvabillModel.pickInvoices(input.Trim(),true);

            // No need to continue if we have nothing to process
            if (invoices.Count() == 0)
            {
                MessageBox.Show("SORRY, BUT THERE IS NO INVOICE WITH NUMBER :" + input + "!");
                return;
            }

            string message = string.Empty;

            // SET INDICATORS

            foreach (CISAvabillModel invoice in invoices)
            {

                // Prepare data before processing it.
                invoice.RECEIPT_TYPE_MRC = "NS";

                message = SDCFactory.bill(invoice);

                if (message.ToLower().Equals("success"))
                {
                    MessageBox.Show("INVOICE WITH NUMBER:" + input + " HAS BEEN PROCESSED SUCCESSFULLY");
                    return;
                }

                // WE HAVE ERROR IN THE REQUEST SHOW THE ERROR
                MessageBox.Show(message + "|INVOICE NUMBER:" + invoice.RECEIPT_NUMBER);
                return;
            }


            MessageBox.Show("TASK HAS BEEN COMPLETED");
        }

        /**
         * Z & X REPORT
         */
        private void xZReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dates = this.getDateRange();
            string startDate = dates["startDate"];
            string endDate = dates["endDate"];

            Reports.getXZReport(startDate, endDate);
        }

        private void pLUReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dates = this.getDateRange();
            string startDate = dates["startDate"];
            string endDate = dates["endDate"];

            Reports.getPLUReport(startDate, endDate);
        }

        /**
         * Get dates
         */
        private Dictionary<string, string> getDateRange()
        {
            string startDate = string.Empty;
            string endDate = string.Empty;

            // Get start date
            while (string.IsNullOrWhiteSpace(startDate))
            {
                startDate = Interaction.InputBox("Enter starting date (Example : 29/8/2016)", "Enter start date", "", -1, -1);
            }

            // Get end date
            while (string.IsNullOrWhiteSpace(endDate))
            {
                endDate = Interaction.InputBox("Enter End date (Example : 30/8/2016)", "Enter End date", "", -1, -1);
            }

            Dictionary<string, string> dates = new Dictionary<string, string>();

            dates.Add("startDate", startDate);
            dates.Add("endDate", endDate);

            return dates;
        }

      
   }
    
}
