﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDCCommunicator.Helpers
{
    class PrintInvoice
    {
        public void pdf(string fileName = "INVOICETEST")
        {

            try
            {
                //Create a New instance on Document Class
                //Read the Data from Input File
                string template = @"InvoiceTemplates\" + fileName;
                StreamReader file = new StreamReader(template);
                // Step 1: Create a System.IO.FileStream object:
                FileStream fs = new FileStream(@"CERTIFIED_INVOICES\" + fileName + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None);

                // Step 2: Create a iTextSharp.text.Document object:
                Rectangle A4 = new Rectangle(PageSize.A4);
                BaseFont bfTimes = BaseFont.CreateFont(BaseFont.COURIER, BaseFont.CP1252, false);
                iTextSharp.text.Font times = new iTextSharp.text.Font(bfTimes, 10);
                iTextSharp.text.Document doc = new iTextSharp.text.Document(A4, 10, 10, 10, 10);
                //Step 3: Create a iTextSharp.text.pdf.PdfWriter object. It helps to write the Document to the Specified FileStream:
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);

                //Step 4: Openning the Document:
                doc.Open();

                //Step 5: Adding a Paragraph by creating a iTextSharp.text.Paragraph object:
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    //leftPadding = (width - line.Length) / 2;
                    //rightPadding = width - line.Length - leftPadding;
                    //line = new string(' ', Convert.ToInt32(leftPadding)) + line + new string(' ', Convert.ToInt32(rightPadding));
                    doc.Add(new iTextSharp.text.Paragraph(line, times));
                }

                //Step 6: Closing the Document:
                doc.Close();

            }
            catch (Exception ex)
            {

            }
        }

        public void formatedInvoice(string signature = "IR84-99TN-FCYY-CE22-4HWE-V5TA-EE",string type = "Normal Sale")
        {
            // Use var type which is shorter.
            using (var writer = new StreamWriter("SampleInvoice.txt"))
            {
                // Setting header
                writer.WriteLine("                   company                      ");
                writer.WriteLine("                   address                      ");
                writer.WriteLine("                     TIN                        ");

                // Receipt Type
                writer.WriteLine("                     COPY                        ");
                writer.WriteLine("-------------------------------------------------");
                writer.WriteLine("                   "+type+"                       ");
                writer.WriteLine("-------------------------------------------------");

                writer.WriteLine("             Welcome to our shop                ");
                writer.WriteLine("          REFUND IS APPROVED ONLY FOr           ");
                writer.WriteLine("             ORIGINAL SALES RECEIPT             ");
                writer.WriteLine("                   Client ID                    ");

                // Starting item lines
                writer.WriteLine("------------------------------------------------");
                writer.WriteLine("ITEM 1                                          ");
                writer.WriteLine("ITEM 2                                          ");
                writer.WriteLine("ITEM 3                                          ");
                writer.WriteLine("ITEM 4                                          ");

                // End items lines
                writer.WriteLine("------------------------------------------------");
                writer.WriteLine("        THIS IS NOT AN OFFICIAL RECEIPT         ");
                writer.WriteLine("------------------------------------------------");

                // TAXES NUMBERS 
                writer.WriteLine("TOTAL                                  -36139.50");
                writer.WriteLine("TOTAL                         B-18.00% -36139.50");
                writer.WriteLine("TOTAL                             TAX B -5512.81");
                writer.WriteLine("TOTAL                               TAX -5512.81");
                writer.WriteLine("------------------------------------------------");

                // Payment methods
                writer.WriteLine("CASH                                   -36139.50");
                writer.WriteLine("ITEMS NUMBER                                   1");
                writer.WriteLine("------------------------------------------------");

                // SDC Information
                writer.WriteLine("              SDC INFORMATION                  ");
                writer.WriteLine("Date: 25/5/2012                  Time: 11:48:27");
                writer.WriteLine("SDC ID:                            SDC001000001");
                writer.WriteLine("RECEIPT NUMBER:                       12/259 NR");
                writer.WriteLine("               Internal Data:                  ");
                writer.WriteLine("        "+signature+"       ");
                writer.WriteLine("             Receipt Signature:                ");
                writer.WriteLine("            669X-TBMM-GPE4-445D                ");
                writer.WriteLine("-----------------------------------------------");
                writer.WriteLine("RECEIPT NUMBER:                             153");
                writer.WriteLine("DATE: 25/5/2012                  TIME: 11:50:24");
                writer.WriteLine("MRC:                                AAACC123456");
                writer.WriteLine("-----------------------------------------------");
                writer.WriteLine("                THANK YOU                      ");
                writer.WriteLine("        WE APPRECIATE YOUR BUSINESS            ");


            }
      
        }
    }
}
